<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Customer Details</div>
		<div class="panel">
			<!--View-->
			<?php
				if($_POST)
				{
					$name = $_POST['name'];
					$phone = $_POST['phone'];
					$email = $_POST['email'];
					$due = $_POST['due'];
					$address = $_POST['address'];
					$type = $_POST['type'];
					$date = date('d/m/Y');
					$date = str_replace('/', '-', $date);
					$date = strtotime($date);
					 $req="INSERT INTO personinformation (date,name, phone, email, address, type,due)  VALUES ('$date','$name', '$phone', '$email', '$address', '$type', $due)";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='customer.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='customer.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<script type="text/javascript">
			$(function () {

				$("#customer_name").autocomplete("customer1.php", {
					width: 160,
					autoFill: true,
					selectFirst: true
				});
				})
			function view_earning(val){
				$('#datatable tr').remove();
				$.POST('view_gift_details.php', {productid: val},
				function (data) {
					$(data).appendTo('#datatable');
				});

				//$('#hidden_field').html('<input type="hidden" name="id" value='+val+' />');
			}
			</script>
			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Customer Name</th>
						<th>Customer Phone</th>
						<th>Customer Email</th>
						<th>Previous Due</th>
						<th>Customer Address</th>
						<th><a href="customer_add.php" id="example1" class="view btn btn-primary">Add Customer</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysqli_query($conn, "SELECT * FROM  personinformation where type='customer' order by id DESC");
					while ($info = mysqli_fetch_array($query))
					{?>
					<tr>
						<td><?php echo $info['name'];?></td>
						<td><?php echo $info['phone'];?></td>
						<td><?php echo $info['email'];?></td>
						<td><?php echo $info['due'];?> Tk </td>
						<td><?php echo $info['address'];?></td>
						<td>
							<span class="pull-right">
								<a title="View" href="customer_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="customer_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="customer_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
