<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Stock Report</div>
		<div class="panel">

			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
			 	</table>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id" class="display table table-bordered">
					<thead>
						<tr>

						<th>Product Name</th>
						<th>Category Name</th>
						<th>Retail Price</th>
						<th>Wholesale Price</th>
						<th>Warranty</th>
						<th>Expiery Date</th>
						<th>Stock Amount</th>
						<th>Action</th>
						</tr>
					</thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{

						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);

					$purchase = mysqli_query($conn, "SELECT * FROM   product_details where product_add_date between '$from' and '$to' order by id desc");
					while ($info = mysqli_fetch_array($purchase))
						{

					?>

					<tr>
						<td><?php echo $info['product_code'];?> &nbsp; <?php echo $info['name'];?></td>
						<td><?php echo $info['category'];?></td>
						<td>Tk  <?php echo $info['sale_price'];?></td>
						<td>Tk  <?php echo $info['wholesale_price'];?></td>
						<td><?php echo $info['warranty'];?></td>
						<td><?php echo $info['expire_date'];?></td>
						<td><?php echo $info['stock_id'];?></td>
						<td width="140">
							<span class="pull-right">
								<a title="View" href="product_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="product_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="product_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php		  } }
					else
					{
						$purchase = mysqli_query($conn, "SELECT * FROM   product_details order by id desc");
					while ($info = mysqli_fetch_array($purchase))
					{

					?>

					<tr>
				<td><?php echo $info['product_code'];?> &nbsp; <?php echo $info['name'];?></td>
						<td><?php echo $info['category'];?></td>
						<td>Tk  <?php echo $info['sale_price'];?></td>
						<td>Tk  <?php echo $info['wholesale_price'];?></td>
						<td><?php echo $info['warranty'];?></td>
						<td><?php echo $info['expire_date'];?></td>
						<td><?php echo $info['stock_id'];?></td>
						<td width="140">
							<span class="pull-right">
								<a title="View" href="product_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="product_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="product_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php    } } ?>
					</tbody>
				</table>
			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
