-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2019 at 05:01 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `password`, `address`, `email`, `phone`, `status`) VALUES
(1, 'Golam Robbani', 'admin', 'admin', '', '', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `bankinformation`
--

CREATE TABLE `bankinformation` (
  `id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `person` varchar(50) NOT NULL,
  `person_type` varchar(20) NOT NULL,
  `bankname` varchar(50) NOT NULL,
  `accountname` varchar(50) NOT NULL,
  `accountnumber` int(11) NOT NULL,
  `comments` text NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bankinformation`
--

INSERT INTO `bankinformation` (`id`, `date`, `person`, `person_type`, `bankname`, `accountname`, `accountnumber`, `comments`, `store_id`) VALUES
(5, '1575482400', 'Robbani', 'Manager', 'DBL', 'Saving', 1254447876, '      ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `barcode_setup`
--

CREATE TABLE `barcode_setup` (
  `id` int(11) NOT NULL,
  `row1` varchar(20) NOT NULL,
  `row2` varchar(20) NOT NULL,
  `row3` varchar(20) NOT NULL,
  `row4` varchar(20) NOT NULL,
  `row5` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barcode_setup`
--

INSERT INTO `barcode_setup` (`id`, `row1`, `row2`, `row3`, `row4`, `row5`) VALUES
(1, 'name', 'brand', 'sale_price', 'sale_price', 'wholesale_price');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(50) NOT NULL,
  `branch_phone` varchar(20) NOT NULL,
  `branch_manager` varchar(40) NOT NULL,
  `branch_email` varchar(100) NOT NULL,
  `branch_address` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `comments` text NOT NULL,
  `description` text NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `category_parent` int(11) NOT NULL,
  `comments` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `category_parent`, `comments`, `store_id`, `description`) VALUES
(51, 'cloth', 0, '', 0, ''),
(52, 'Food', 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `demage`
--

CREATE TABLE `demage` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `comments` text NOT NULL,
  `date` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demage`
--

INSERT INTO `demage` (`id`, `name`, `code`, `product_id`, `stock`, `comments`, `date`) VALUES
(6, 'Shirt', 'shirt123', 241, 2, 'color problem', '1575482400');

-- --------------------------------------------------------

--
-- Table structure for table `earning`
--

CREATE TABLE `earning` (
  `id` int(11) NOT NULL,
  `date` text NOT NULL,
  `earning_head` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `store_id` int(11) NOT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `earning`
--

INSERT INTO `earning` (`id`, `date`, `earning_head`, `amount`, `store_id`, `comments`) VALUES
(17, '1575482400', 'others', 2000, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `earning_head`
--

CREATE TABLE `earning_head` (
  `id` int(11) NOT NULL,
  `head` varchar(40) NOT NULL,
  `store_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `earning_head`
--

INSERT INTO `earning_head` (`id`, `head`, `store_id`, `description`, `comments`) VALUES
(17, 'earning name', 0, 'earning description', 'earning comments'),
(18, 'others', 0, '', ''),
(19, 'ghfgh', 0, 'kjjjkjk', 'hhj');

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(11) NOT NULL,
  `date` text NOT NULL,
  `expense_head` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `store_id` int(11) NOT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `date`, `expense_head`, `amount`, `store_id`, `comments`) VALUES
(265, '1575482400', 'Transport', 1000, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `expense_head`
--

CREATE TABLE `expense_head` (
  `id` int(11) NOT NULL,
  `head` varchar(40) NOT NULL,
  `store_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_head`
--

INSERT INTO `expense_head` (`id`, `head`, `store_id`, `comments`, `description`) VALUES
(35, 'Transport', 0, '', ''),
(36, 'expense name', 0, 'expense comments', 'expense descrioption');

-- --------------------------------------------------------

--
-- Table structure for table `gift`
--

CREATE TABLE `gift` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `card_number` varchar(20) NOT NULL,
  `amount` float NOT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investment`
--

CREATE TABLE `investment` (
  `id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `investor` varchar(100) NOT NULL,
  `amount` float NOT NULL,
  `comments` text NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investment`
--

INSERT INTO `investment` (`id`, `date`, `investor`, `amount`, `comments`, `store_id`) VALUES
(5, '1575482400', 'shop worner', 5000, '', 0),
(7, '1575568800', 'shop worner', 10000, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE `loan` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `interest` float NOT NULL,
  `payable_amount` float NOT NULL,
  `return_date` varchar(50) NOT NULL,
  `period` varchar(20) NOT NULL,
  `type` varchar(10) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `bank_ac_name` varchar(20) NOT NULL,
  `branch_name` varchar(150) NOT NULL,
  `total_installment` varchar(10) NOT NULL,
  `monthly_payment` varchar(20) NOT NULL,
  `bank_ac_name_name` varchar(50) NOT NULL,
  `comments` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_pay`
--

CREATE TABLE `loan_pay` (
  `id` int(11) NOT NULL,
  `pay_to` text NOT NULL,
  `amount` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_type`
--

CREATE TABLE `loan_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `login_details`
--

CREATE TABLE `login_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `login_time` varchar(255) NOT NULL,
  `logout_time` varchar(255) NOT NULL,
  `time` varchar(50) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_details`
--

INSERT INTO `login_details` (`id`, `user_id`, `date`, `login_time`, `logout_time`, `time`, `store_id`) VALUES
(1, 23, '16/01/2019', '08:35:12pm', '08:35:43pm', '', 0),
(2, 23, '16/01/2019', '08:35:51pm', '08:48:04pm', '', 0),
(3, 23, '16/01/2019', '08:53:18pm', '09:21:24pm', '', 0),
(4, 23, '17/01/2019', '10:37:19pm', '10:50:25pm', '', 0),
(5, 23, '18/01/2019', '12:12:02am', '', '', 0),
(6, 23, '18/01/2019', '12:17:20pm', '12:30:03pm', '', 0),
(7, 23, '18/01/2019', '01:10:07pm', '', '', 0),
(8, 23, '18/01/2019', '01:10:08pm', '', '', 0),
(9, 23, '18/01/2019', '01:16:04pm', '', '', 0),
(10, 23, '18/01/2019', '08:26:47pm', '', '', 0),
(11, 23, '18/01/2019', '08:59:28pm', '', '', 0),
(12, 23, '18/01/2019', '09:11:47pm', '', '', 0),
(13, 23, '18/01/2019', '09:47:51pm', '', '', 0),
(14, 23, '18/01/2019', '09:50:33pm', '', '', 0),
(15, 23, '18/01/2019', '09:50:34pm', '', '', 0),
(16, 23, '19/01/2019', '12:59:50pm', '', '', 0),
(17, 23, '19/01/2019', '01:33:26pm', '01:52:50pm', '', 0),
(18, 23, '27/01/2019', '09:18:22pm', '', '', 0),
(19, 23, '06/02/2019', '11:57:53pm', '', '', 0),
(20, 23, '09/02/2019', '11:24:26am', '', '', 0),
(21, 23, '10/02/2019', '10:51:38pm', '', '', 0),
(22, 23, '11/02/2019', '04:09:55pm', '', '', 0),
(23, 23, '12/02/2019', '03:01:37pm', '', '', 0),
(24, 23, '30/04/2019', '11:31:26pm', '', '', 0),
(25, 23, '01/05/2019', '10:55:16am', '11:00:24am', '', 0),
(26, 23, '01/05/2019', '11:00:26am', '05:05:06pm', '', 0),
(27, 23, '01/05/2019', '05:05:10pm', '', '', 0),
(28, 23, '02/05/2019', '06:20:51pm', '', '', 0),
(29, 23, '04/05/2019', '12:56:30pm', '', '', 0),
(30, 23, '05/05/2019', '08:56:50pm', '', '', 0),
(31, 23, '07/05/2019', '10:27:01pm', '', '', 0),
(32, 23, '11/05/2019', '02:50:23pm', '', '', 0),
(33, 23, '12/05/2019', '08:43:52pm', '', '', 0),
(34, 23, '19/05/2019', '10:13:57am', '08:29:25pm', '', 0),
(35, 23, '30/11/2019', '12:42:58pm', '12:44:05pm', '', 0),
(36, 23, '30/11/2019', '12:44:27pm', '', '', 0),
(37, 23, '30/11/2019', '05:09:40pm', '05:15:15pm', '', 0),
(38, 23, '30/11/2019', '11:04:05pm', '11:04:15pm', '', 0),
(39, 23, '30/11/2019', '11:04:19pm', '', '', 0),
(40, 23, '30/11/2019', '11:31:55pm', '11:34:13pm', '', 0),
(41, 23, '30/11/2019', '11:34:18pm', '11:35:33pm', '', 0),
(42, 23, '30/11/2019', '11:35:44pm', '12:20:14am', '', 0),
(43, 23, '01/12/2019', '12:20:35am', '', '', 0),
(44, 23, '01/12/2019', '01:09:07am', '02:20:05am', '', 0),
(45, 23, '01/12/2019', '09:21:50pm', '09:22:33pm', '', 0),
(46, 23, '01/12/2019', '09:26:08pm', '', '', 0),
(47, 23, '02/12/2019', '10:18:10am', '', '', 0),
(48, 23, '02/12/2019', '10:47:14pm', '', '', 0),
(49, 23, '03/12/2019', '09:46:52pm', '', '', 0),
(50, 23, '03/12/2019', '10:00:38pm', '11:53:30pm', '', 0),
(51, 23, '03/12/2019', '11:54:21pm', '12:13:52am', '', 0),
(52, 23, '04/12/2019', '05:17:48pm', '05:27:38pm', '', 0),
(53, 23, '04/12/2019', '05:28:08pm', '05:30:03pm', '', 0),
(54, 23, '04/12/2019', '05:31:03pm', '', '', 0),
(55, 23, '04/12/2019', '10:17:41pm', '12:34:45am', '', 0),
(56, 23, '05/12/2019', '12:35:03am', '12:37:25am', '', 0),
(57, 23, '05/12/2019', '12:40:00am', '04:05:32pm', '', 0),
(58, 23, '05/12/2019', '04:06:14pm', '04:08:23pm', '', 0),
(59, 23, '05/12/2019', '04:08:40pm', '04:23:39pm', '', 0),
(60, 23, '05/12/2019', '04:23:41pm', '04:47:00pm', '', 0),
(61, 23, '05/12/2019', '04:47:43pm', '06:43:11pm', '', 0),
(62, 23, '05/12/2019', '06:44:09pm', '08:22:49pm', '', 0),
(63, 23, '05/12/2019', '08:24:34pm', '08:52:22pm', '', 0),
(64, 23, '05/12/2019', '08:53:44pm', '08:54:43pm', '', 0),
(65, 23, '05/12/2019', '08:55:22pm', '08:58:30pm', '', 0),
(66, 23, '05/12/2019', '08:58:32pm', '08:58:55pm', '', 0),
(67, 23, '05/12/2019', '09:00:02pm', '09:00:41pm', '', 0),
(68, 23, '05/12/2019', '09:01:20pm', '09:39:19pm', '', 0),
(69, 23, '05/12/2019', '10:20:05pm', '', '', 0),
(70, 23, '06/12/2019', '10:31:33am', '10:31:54am', '', 0),
(71, 23, '06/12/2019', '10:32:12am', '10:41:53am', '', 0),
(72, 23, '06/12/2019', '10:49:00am', '', '', 0),
(73, 23, '06/12/2019', '12:25:15pm', '01:07:05pm', '', 0),
(74, 23, '06/12/2019', '01:09:13pm', '07:02:50pm', '', 0),
(75, 23, '06/12/2019', '07:02:52pm', '', '', 0),
(76, 23, '06/12/2019', '07:03:26pm', '07:03:48pm', '', 0),
(77, 23, '06/12/2019', '07:03:52pm', '07:05:32pm', '', 0),
(78, 23, '06/12/2019', '07:06:32pm', '07:06:46pm', '', 0),
(79, 23, '06/12/2019', '07:07:26pm', '07:07:34pm', '', 0),
(80, 23, '06/12/2019', '07:09:07pm', '', '', 0),
(81, 23, '06/12/2019', '08:08:52pm', '09:27:53pm', '', 0),
(82, 29, '06/12/2019', '09:28:02pm', '09:28:11pm', '', 0),
(83, 29, '06/12/2019', '09:28:57pm', '09:30:53pm', '', 0),
(84, 30, '06/12/2019', '09:31:04pm', '09:31:28pm', '', 0),
(85, 23, '06/12/2019', '09:33:20pm', '09:35:36pm', '', 0),
(86, 23, '06/12/2019', '09:35:38pm', '09:59:19pm', '', 0),
(87, 23, '06/12/2019', '09:59:21pm', '10:01:14pm', '', 0),
(88, 23, '06/12/2019', '10:01:35pm', '10:02:23pm', '', 0),
(89, 23, '06/12/2019', '10:02:25pm', '11:23:01pm', '', 0),
(90, 23, '06/12/2019', '11:23:20pm', '11:33:35am', '', 0),
(91, 23, '07/12/2019', '11:34:21am', '11:42:22am', '', 0),
(92, 23, '07/12/2019', '11:42:24am', '12:06:00pm', '', 0),
(93, 23, '07/12/2019', '12:06:17pm', '', '', 0),
(94, 23, '07/12/2019', '01:36:52pm', '', '', 0),
(95, 23, '07/12/2019', '03:40:42pm', '03:41:16pm', '', 0),
(96, 23, '07/12/2019', '03:55:52pm', '03:58:58pm', '', 0),
(97, 23, '07/12/2019', '03:59:01pm', '', '', 0),
(98, 23, '10/12/2019', '08:14:41pm', '', '', 0),
(99, 23, '12/12/2019', '01:54:40pm', '01:55:13pm', '', 0),
(100, 23, '12/12/2019', '01:55:15pm', '01:55:17pm', '', 0),
(101, 23, '12/12/2019', '01:55:19pm', '01:59:07pm', '', 0),
(102, 23, '12/12/2019', '01:59:17pm', '02:00:33pm', '', 0),
(103, 23, '12/12/2019', '02:00:38pm', '02:02:53pm', '', 0),
(104, 23, '12/12/2019', '02:03:36pm', '', '', 0),
(105, 23, '12/12/2019', '02:24:20pm', '', '', 0),
(106, 23, '12/12/2019', '03:01:12pm', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`id`, `name`, `value`) VALUES
(1, 'January', 'Jan'),
(2, 'February', 'Feb'),
(3, 'March', 'Mar'),
(4, 'April', 'Apr'),
(5, 'May', 'May'),
(6, 'June', 'Jun'),
(7, 'July', 'Jul'),
(8, 'August', 'Aug'),
(9, 'September', 'Sep'),
(10, 'October', 'Oct'),
(11, 'November', 'Nov'),
(12, 'December', 'Dec');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `date` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `payable` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `due` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `bank_name` varchar(225) NOT NULL,
  `branch_name` varchar(225) NOT NULL,
  `chaque_no` varchar(255) NOT NULL,
  `dated` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_bank` int(11) NOT NULL,
  `payment_customer` int(11) NOT NULL,
  `note` text NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`date`, `name`, `payable`, `paid`, `due`, `payment`, `payment_type`, `bank_name`, `branch_name`, `chaque_no`, `dated`, `supplier_id`, `order_id`, `payment_bank`, `payment_customer`, `note`, `id`) VALUES
('1575482400', 'Korim', 360, 300, '60', '60', 'cash', 'NA', 'NA', 'NA', 0, 118, 22, 5, 115, '', 29);

-- --------------------------------------------------------

--
-- Table structure for table `personinformation`
--

CREATE TABLE `personinformation` (
  `id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `comments` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `designation` varchar(30) NOT NULL,
  `salary` float NOT NULL,
  `due` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personinformation`
--

INSERT INTO `personinformation` (`id`, `date`, `name`, `address`, `phone`, `email`, `company_name`, `type`, `comments`, `store_id`, `designation`, `salary`, `due`) VALUES
(63, '1575223200', 'Golam Robbani', 'House: #40 ,Road: Jomoj Road ,Shewra Bazar Dhaka(1206)', '01759389686', 'golamrobbani29@gmail.com', '', 'employee', '', 0, 'Web Developer', 25000, 0),
(110, '1575223200', 'zafor', 'dhaka', '01868250676', 'arifkhan808808@gmail.com', '', 'employee', '', 0, 'python developer', 29000, 0),
(113, '1575309600', 'md.Arif', 'Gulshan', '01558854', 'arif@gmail.com', '', 'employee', '', 0, 'Sals Man', 18700, 0),
(115, '1575482400', 'Asif', 'Shewra Bazar Dhaka(1206)', '01759389686', 'asif12@gmail.com', '', 'customer', '', 0, '', 0, 0),
(116, '1575482400', 'Rahim', 'maniknagar,Dhaka', '01759389682', 'rohim@gmail.com', '', 'Supplier', '', 0, '', 0, 0),
(118, '1575482400', 'Korim', 'bardhara,dhaka', '01759389686', 'korim123@gmail.com', '', 'Supplier', '', 0, '', 0, 0),
(119, '1575482400', 'robiul', 'dhaka', '01759389683', 'robiul123@gmail.com', '', 'customer', '', 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `product_code` varchar(100) NOT NULL,
  `product_color` varchar(100) NOT NULL,
  `product_size` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `lot_no` int(11) NOT NULL,
  `purchase_cost` int(50) NOT NULL,
  `sale_price` int(50) NOT NULL,
  `wholesale_price` int(50) NOT NULL,
  `unit_type` varchar(10) NOT NULL,
  `per_unit` int(11) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `vat` varchar(10) NOT NULL,
  `warranty` varchar(10) NOT NULL,
  `expire_date` text NOT NULL,
  `product_add_date` text NOT NULL,
  `product_update_date` text NOT NULL,
  `comments` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `stock_id` varchar(255) NOT NULL,
  `minquantity` int(50) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`id`, `name`, `product_code`, `product_color`, `product_size`, `category`, `lot_no`, `purchase_cost`, `sale_price`, `wholesale_price`, `unit_type`, `per_unit`, `brand`, `description`, `vat`, `warranty`, `expire_date`, `product_add_date`, `product_update_date`, `comments`, `status`, `stock_id`, `minquantity`, `image`) VALUES
(241, 'Shirt', 'shirt123', 'Red', 'M', 'cloth', 1, 200, 300, 250, 'pcs', 1, '', 'description', '', '', '2019-12-10', '05-12-2019', '', '', 'active', '8', 0, ''),
(242, 'Pant', 'p123', 'blue', 'M', 'cloth', 2, 500, 600, 550, 'pcs', 1, '', '', '', '', '2019-12-06', '06-12-2019', '', '', 'active', '21', 0, ''),
(243, 'T-shirt', 't123', 'blue', 'M', 'cloth', 1, 200, 300, 250, 'pcs', 1, '', '', '', '', '2019-12-28', '12-12-2019', '', '', 'active', '20', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_qty`
--

CREATE TABLE `product_qty` (
  `id` int(11) NOT NULL,
  `size` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` float NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_qty`
--

INSERT INTO `product_qty` (`id`, `size`, `color`, `qty`, `price`, `product_id`) VALUES
(1, '128-176', 'Black', 44, 4, 222),
(2, 'SM', 'Pink', 22, 5, 222),
(3, 'L', 'Blue', 11, 6, 222);

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `date` varchar(255) NOT NULL,
  `supplier_id` varchar(255) NOT NULL,
  `supplier` varchar(255) NOT NULL DEFAULT 'N/A',
  `address` varchar(255) NOT NULL DEFAULT 'N/A',
  `contact` varchar(255) NOT NULL DEFAULT 'N/A',
  `gtotal` varchar(255) NOT NULL,
  `payable` varchar(255) NOT NULL,
  `discount` int(255) NOT NULL DEFAULT '0',
  `paid` varchar(255) NOT NULL,
  `cammount` varchar(255) NOT NULL DEFAULT '0',
  `pmethod` varchar(255) NOT NULL,
  `pname` varchar(255) NOT NULL,
  `pnumber` varchar(255) NOT NULL,
  `due` varchar(255) NOT NULL DEFAULT '0',
  `note` text NOT NULL,
  `reciveby` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `payment_bank` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`date`, `supplier_id`, `supplier`, `address`, `contact`, `gtotal`, `payable`, `discount`, `paid`, `cammount`, `pmethod`, `pname`, `pnumber`, `due`, `note`, `reciveby`, `status`, `payment_bank`, `id`) VALUES
('1575482400', '118', 'Korim', 'bardhara,dhaka', '01759389686', '400', '360', 40, '300', '', 'cash', '0', '0', '60', '0', 'Golam Robbani', 1, 0, 22);

-- --------------------------------------------------------

--
-- Table structure for table `purchases_product`
--

CREATE TABLE `purchases_product` (
  `date` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quty` varchar(255) NOT NULL,
  `sell` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL DEFAULT '0',
  `total` varchar(255) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `per_unit` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `pcode` varchar(255) NOT NULL,
  `ppid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases_product`
--

INSERT INTO `purchases_product` (`date`, `name`, `quty`, `sell`, `discount`, `total`, `sid`, `unit`, `per_unit`, `supplier_id`, `item_id`, `pcode`, `ppid`) VALUES
('1575482400', 'Shirt', '2', '200', '0', '400', '22', 'pcs', 1, 118, 241, 'shirt123', 32);

-- --------------------------------------------------------

--
-- Table structure for table `purchases_return`
--

CREATE TABLE `purchases_return` (
  `date` varchar(255) NOT NULL,
  `supplier_id` varchar(255) NOT NULL,
  `supplier` varchar(255) NOT NULL DEFAULT 'N/A',
  `address` varchar(255) NOT NULL DEFAULT 'N/A',
  `contact` varchar(255) NOT NULL DEFAULT 'N/A',
  `gtotal` varchar(255) NOT NULL,
  `payable` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL DEFAULT '0.00',
  `paid` varchar(255) NOT NULL,
  `cammount` varchar(255) NOT NULL DEFAULT '0.00',
  `pmethod` varchar(255) NOT NULL,
  `pname` varchar(255) NOT NULL,
  `pnumber` varchar(255) NOT NULL,
  `due` varchar(255) NOT NULL DEFAULT '0.00',
  `note` text NOT NULL,
  `reciveby` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `recive_bank` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases_return`
--

INSERT INTO `purchases_return` (`date`, `supplier_id`, `supplier`, `address`, `contact`, `gtotal`, `payable`, `discount`, `paid`, `cammount`, `pmethod`, `pname`, `pnumber`, `due`, `note`, `reciveby`, `status`, `recive_bank`, `id`) VALUES
('1575568800', '118', 'Korim', 'bardhara,dhaka', '01759389686', '500', '450', '50', '450', '', 'cash', '0', '0', '0', '0', 'Golam Robbani', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchases_return_product`
--

CREATE TABLE `purchases_return_product` (
  `date` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quty` varchar(255) NOT NULL,
  `sell` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL DEFAULT '0',
  `total` varchar(255) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `per_unit` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `pcode` varchar(255) NOT NULL,
  `ppid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases_return_product`
--

INSERT INTO `purchases_return_product` (`date`, `name`, `quty`, `sell`, `discount`, `total`, `sid`, `unit`, `per_unit`, `supplier_id`, `item_id`, `pcode`, `ppid`) VALUES
('', 'Pant', '1', '500', '0', '500', '1', 'pcs', 1, 118, 242, 'p123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `recive`
--

CREATE TABLE `recive` (
  `date` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `payable` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `due` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `chaque_no` varchar(255) NOT NULL,
  `dated` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `recive_bank` int(11) NOT NULL,
  `note` text NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recive`
--

INSERT INTO `recive` (`date`, `name`, `payable`, `paid`, `due`, `payment`, `payment_type`, `bank_name`, `branch_name`, `chaque_no`, `dated`, `customer_id`, `order_id`, `recive_bank`, `note`, `id`) VALUES
('1575482400', 'robiul', 540, 500, '40', '40', 'cash', 'NA', 'NA', 'NA', 0, 119, 47, 5, '', 61);

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(11) NOT NULL,
  `month` varchar(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `year` varchar(10) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `store_id` int(11) NOT NULL,
  `mode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `employee_id`, `month`, `date`, `year`, `amount`, `notes`, `store_id`, `mode`) VALUES
(2, '63', 'Dec', '05/12/2019', '2019', '10000', '', 0, 'Cash');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `date` varchar(255) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `customer` varchar(255) NOT NULL DEFAULT 'N/A',
  `address` varchar(255) NOT NULL DEFAULT 'N/A',
  `contact` varchar(255) NOT NULL DEFAULT 'N/A',
  `cost` varchar(100) NOT NULL,
  `gtotal` varchar(255) NOT NULL,
  `payable` varchar(255) NOT NULL,
  `discount` int(255) NOT NULL DEFAULT '0',
  `paid` varchar(255) NOT NULL,
  `cammount` varchar(255) NOT NULL DEFAULT '0',
  `pmethod` varchar(255) NOT NULL,
  `pname` varchar(255) NOT NULL,
  `pnumber` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `due` varchar(255) NOT NULL DEFAULT '0',
  `saleby` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `recive_bank` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`date`, `customer_id`, `customer`, `address`, `contact`, `cost`, `gtotal`, `payable`, `discount`, `paid`, `cammount`, `pmethod`, `pname`, `pnumber`, `note`, `due`, `saleby`, `type`, `status`, `recive_bank`, `id`) VALUES
('1575309600', '', '', '', '', '450', '520', '520', 0, '520', '0.00', 'card', 'UCB', '5429855546', 'ok', '0.00', 'Golam Robbani', 'sale', '0', 0, 43),
('1575482400', '119', 'robiul', 'dhaka', '01759389683', '400', '600', '540', 60, '500', '0.00', 'cash', '0', 'N/A', '0', '40.00', 'Golam Robbani', 'sale', '1', 0, 47);

-- --------------------------------------------------------

--
-- Table structure for table `sales_product`
--

CREATE TABLE `sales_product` (
  `date` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quty` varchar(255) NOT NULL,
  `cost` varchar(255) NOT NULL,
  `sell` varchar(255) NOT NULL,
  `discount` varchar(100) NOT NULL,
  `total` varchar(255) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `ppid` int(11) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `per_unit` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `pcode` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_product`
--

INSERT INTO `sales_product` (`date`, `name`, `quty`, `cost`, `sell`, `discount`, `total`, `sid`, `ppid`, `unit`, `per_unit`, `customer_id`, `item_id`, `pcode`) VALUES
('1575309600', 'Beef', '1', '', '520', '0.00', '520', '43', 57, '2', 1, 0, 230, 'BF001'),
('1575482400', 'Shirt', '2', '', '300', '0.00', '600', '47', 61, 'pcs', 1, 119, 241, 'shirt123');

-- --------------------------------------------------------

--
-- Table structure for table `sales_return`
--

CREATE TABLE `sales_return` (
  `date` varchar(255) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `customer` varchar(255) NOT NULL DEFAULT 'N/A',
  `address` varchar(255) NOT NULL DEFAULT 'N/A',
  `contact` varchar(255) NOT NULL DEFAULT 'N/A',
  `cost` varchar(100) NOT NULL,
  `gtotal` varchar(255) NOT NULL,
  `payable` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL DEFAULT '0.00',
  `paid` varchar(255) NOT NULL,
  `cammount` varchar(255) NOT NULL DEFAULT '0.00',
  `pmethod` varchar(255) NOT NULL,
  `pname` varchar(255) NOT NULL,
  `pnumber` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `due` varchar(255) NOT NULL DEFAULT '0.00',
  `saleby` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `payment_bank` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_return`
--

INSERT INTO `sales_return` (`date`, `customer_id`, `customer`, `address`, `contact`, `cost`, `gtotal`, `payable`, `discount`, `paid`, `cammount`, `pmethod`, `pname`, `pnumber`, `note`, `due`, `saleby`, `type`, `status`, `payment_bank`, `id`) VALUES
('1575482400', '119', 'robiul', 'dhaka', '01759389683', '200', '300', '270', '30', '270', '0.00', 'cash', '0', 'N/A', '0', '0.00', 'Golam Robbani', 'sale', 1, 0, 4),
('1575568800', '119', 'robiul', 'dhaka', '01759389683', '1000', '1200', '1080', '120', '1080', '0.00', 'cash', '0', 'N/A', '0', '0.00', 'Golam Robbani', 'sale', 1, 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sales_return_product`
--

CREATE TABLE `sales_return_product` (
  `date` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quty` varchar(255) NOT NULL,
  `cost` varchar(255) NOT NULL,
  `sell` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL DEFAULT '0',
  `total` varchar(255) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `per_unit` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `pcode` varchar(255) NOT NULL,
  `ppid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_return_product`
--

INSERT INTO `sales_return_product` (`date`, `name`, `quty`, `cost`, `sell`, `discount`, `total`, `sid`, `unit`, `per_unit`, `customer_id`, `item_id`, `pcode`, `ppid`) VALUES
('1575482400', 'Shirt', '1', '', '300', '0.00', '300', '4', 'pcs', 1, 119, 241, 'shirt123', 4),
('1575568800', 'Pant', '2', '', '600', '0.00', '1200', '5', 'pcs', 1, 119, 242, 'p123', 5);

-- --------------------------------------------------------

--
-- Table structure for table `sale_temp`
--

CREATE TABLE `sale_temp` (
  `pid` varchar(255) NOT NULL,
  `quty` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `site_name` varchar(500) NOT NULL,
  `site_title` varchar(500) NOT NULL,
  `image` varchar(500) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `quantity` int(11) NOT NULL,
  `category` varchar(200) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storeconfiguration`
--

CREATE TABLE `storeconfiguration` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `wholesale_price` int(11) NOT NULL,
  `brand_name` int(11) NOT NULL,
  `vat` int(11) NOT NULL,
  `warranty` int(11) NOT NULL,
  `expiery_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `storeinformation`
--

CREATE TABLE `storeinformation` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `website` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `ownar_name` varchar(255) NOT NULL,
  `ownar_phone` varchar(255) NOT NULL,
  `ownar_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `storeinformation`
--

INSERT INTO `storeinformation` (`id`, `name`, `logo`, `website`, `email`, `phone`, `address`, `ownar_name`, `ownar_phone`, `ownar_address`) VALUES
(2, 'Demo store', '1575484846.png', 'demo.com', 'demo@gmail.com', '03232323232', 'demo store address', 'Demo', '056-6274393', 'demo store address');

-- --------------------------------------------------------

--
-- Table structure for table `store_login`
--

CREATE TABLE `store_login` (
  `id` int(11) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `store_id` varchar(10) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `pay_to` int(11) NOT NULL,
  `pay_amount` float NOT NULL,
  `date` date NOT NULL,
  `payment_type` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `store_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transfer`
--

CREATE TABLE `transfer` (
  `id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `ac_name` varchar(40) NOT NULL,
  `ac_number` varchar(40) NOT NULL,
  `amount` float NOT NULL,
  `comments` text NOT NULL,
  `type` varchar(30) NOT NULL,
  `bankid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transfer`
--

INSERT INTO `transfer` (`id`, `date`, `bank`, `bank_name`, `ac_name`, `ac_number`, `amount`, `comments`, `type`, `bankid`) VALUES
(1, '1575568800', '5', '', '', '', 500, 'fdgs', 'c2b', 0),
(2, '1575568800', '5', '', '', '', 1000, '', 'c2b', 0),
(3, '1575568800', '5', '', '', '', 500, '', 'b2c', 0);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `store_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `name`, `store_id`, `comments`, `description`) VALUES
(5, 'pcs', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `usertype` varchar(40) NOT NULL,
  `employee_name` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `usertype`, `employee_name`, `email`) VALUES
(23, 'admin', '!admin123!', 'admin', 'Golam Robbani', 'golamrobbani29@gmail.com'),
(29, 'admin', 'admin', 'user', 'Nurul', 'golamrobbani29@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE `user_access` (
  `user_id` varchar(255) NOT NULL,
  `access` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`user_id`, `access`, `id`) VALUES
('29', '', 940),
('29', '/pos/dashboard.php', 941),
('29', '/pos/stock.php', 942),
('29', '/pos/low.php', 943),
('29', '/pos/damage.php', 944),
('29', '/pos/purchase.php', 945),
('29', '/pos/purchase_return.php', 946),
('29', '/pos/sale.php', 947),
('29', '/pos/sale_return.php', 948),
('29', '/pos/invoice_free.php', 949),
('29', '/pos/earnhead.php', 950),
('29', '/pos/earning.php', 951),
('29', '/pos/expensehead.php', 952),
('29', '/pos/expense.php', 953),
('29', '/pos/bank.php', 954),
('29', '/pos/investment.php', 955),
('29', '/pos/payment.php', 956),
('29', '/pos/outstanding.php', 957),
('29', '/pos/personal_loan.php', 958),
('29', '/pos/bank_loan.php', 959),
('29', '/pos/paypersonalloan.php', 960),
('29', '/pos/paybankloan.php', 961),
('29', '/pos/c2b.php', 962),
('29', '/pos/b2c.php', 963),
('29', '/pos/salary_payment.php', 964),
('29', '/pos/purchase_report.php', 965),
('29', '/pos/report_purchase_return.php', 966),
('29', '/pos/report_sales.php', 967),
('29', '/pos/report_sales_return.php', 968),
('29', '/pos/report_receive.php', 969),
('29', '/pos/report_receive_due.php', 970),
('29', '/pos/report_payment.php', 971),
('29', '/pos/report_payment_due.php', 972),
('29', '/pos/report_customer.php', 973),
('29', '/pos/report_supplier.php', 974),
('29', '/pos/report_employee.php', 975),
('29', '/pos/salary_report.php', 976),
('29', '/pos/report_expense.php', 977),
('29', '/pos/report_earn.php', 978),
('29', '/pos/low_stock_report.php', 979),
('29', '/pos/stock_report.php', 980),
('29', '/pos/profit_lose.php', 981),
('29', '/pos/report_user_log.php', 982),
('29', '/pos/customer_ledger.php', 983),
('29', '/pos/supplier_ledger.php', 984),
('29', '/pos/shop.php', 985),
('29', '/pos/branches.php', 986),
('29', '/pos/customer.php', 987),
('29', '/pos/supplier.php', 988),
('29', '/pos/investor.php', 989),
('29', '/pos/employee.php', 990),
('29', '/pos/product.php', 991),
('29', '/pos/inactive.php', 992),
('29', '/pos/category.php', 993),
('29', '/pos/brand.php', 994),
('29', '/pos/unit.php,', 995),
('29', '/pos/dynamic_field.php', 996),
('29', '/pos/barcode_setting.php', 997),
('29', '/pos/barcode.php', 998),
('29', '/pos/users.php', 999),
('29', '/pos/backup.php,online.php', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `vat`
--

CREATE TABLE `vat` (
  `id` int(11) NOT NULL,
  `vat_name` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  `store_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `status` varchar(200) NOT NULL,
  `tax_reg` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vat_status`
--

CREATE TABLE `vat_status` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vat_status`
--

INSERT INTO `vat_status` (`id`, `name`) VALUES
(0, 'Active'),
(1, 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `wonerinformation`
--

CREATE TABLE `wonerinformation` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wonerinformation`
--

INSERT INTO `wonerinformation` (`id`, `name`, `address`, `phone`, `email`, `store_id`) VALUES
(1, 'Demo Owner', 'Demo owner address', '01711111111', 'owner@gmail.com', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bankinformation`
--
ALTER TABLE `bankinformation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barcode_setup`
--
ALTER TABLE `barcode_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demage`
--
ALTER TABLE `demage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `earning`
--
ALTER TABLE `earning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `earning_head`
--
ALTER TABLE `earning_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_head`
--
ALTER TABLE `expense_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gift`
--
ALTER TABLE `gift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment`
--
ALTER TABLE `investment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_pay`
--
ALTER TABLE `loan_pay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_type`
--
ALTER TABLE `loan_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_details`
--
ALTER TABLE `login_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personinformation`
--
ALTER TABLE `personinformation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_qty`
--
ALTER TABLE `product_qty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases_product`
--
ALTER TABLE `purchases_product`
  ADD PRIMARY KEY (`ppid`);

--
-- Indexes for table `purchases_return`
--
ALTER TABLE `purchases_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases_return_product`
--
ALTER TABLE `purchases_return_product`
  ADD PRIMARY KEY (`ppid`);

--
-- Indexes for table `recive`
--
ALTER TABLE `recive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_product`
--
ALTER TABLE `sales_product`
  ADD PRIMARY KEY (`ppid`);

--
-- Indexes for table `sales_return`
--
ALTER TABLE `sales_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_return_product`
--
ALTER TABLE `sales_return_product`
  ADD PRIMARY KEY (`ppid`);

--
-- Indexes for table `sale_temp`
--
ALTER TABLE `sale_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `storeconfiguration`
--
ALTER TABLE `storeconfiguration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `storeinformation`
--
ALTER TABLE `storeinformation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_login`
--
ALTER TABLE `store_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer`
--
ALTER TABLE `transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access`
--
ALTER TABLE `user_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vat`
--
ALTER TABLE `vat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vat_status`
--
ALTER TABLE `vat_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wonerinformation`
--
ALTER TABLE `wonerinformation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bankinformation`
--
ALTER TABLE `bankinformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `barcode_setup`
--
ALTER TABLE `barcode_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `demage`
--
ALTER TABLE `demage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `earning`
--
ALTER TABLE `earning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `earning_head`
--
ALTER TABLE `earning_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT for table `expense_head`
--
ALTER TABLE `expense_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `gift`
--
ALTER TABLE `gift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investment`
--
ALTER TABLE `investment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_pay`
--
ALTER TABLE `loan_pay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_type`
--
ALTER TABLE `loan_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_details`
--
ALTER TABLE `login_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `month`
--
ALTER TABLE `month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `personinformation`
--
ALTER TABLE `personinformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;

--
-- AUTO_INCREMENT for table `product_qty`
--
ALTER TABLE `product_qty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `purchases_product`
--
ALTER TABLE `purchases_product`
  MODIFY `ppid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `purchases_return`
--
ALTER TABLE `purchases_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchases_return_product`
--
ALTER TABLE `purchases_return_product`
  MODIFY `ppid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `recive`
--
ALTER TABLE `recive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `sales_product`
--
ALTER TABLE `sales_product`
  MODIFY `ppid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `sales_return`
--
ALTER TABLE `sales_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sales_return_product`
--
ALTER TABLE `sales_return_product`
  MODIFY `ppid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sale_temp`
--
ALTER TABLE `sale_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `storeconfiguration`
--
ALTER TABLE `storeconfiguration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `storeinformation`
--
ALTER TABLE `storeinformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `store_login`
--
ALTER TABLE `store_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transfer`
--
ALTER TABLE `transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user_access`
--
ALTER TABLE `user_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT for table `vat`
--
ALTER TABLE `vat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vat_status`
--
ALTER TABLE `vat_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wonerinformation`
--
ALTER TABLE `wonerinformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
