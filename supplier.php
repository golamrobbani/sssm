<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Supplier Details</div>
		<div class="panel">
			<!--View-->
			<?php
				if($_POST)
				{
					$name = $_POST['name'];
					$phone = $_POST['phone'];
					$due = $_POST['due'];
					$email = $_POST['email'];
					$address = $_POST['address'];
					$date = date('d-m-Y');
					$date = strtotime($date);
					$type = $_POST['type'];
					$req="INSERT INTO personinformation (date, name, phone, email, address, type,due)  VALUES ('$date','$name', '$phone', '$email', '$address', '$type',$due)";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='supplier.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='supplier.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Supplier Name</th>
						<th>Supplier Phone</th>
						<th>Supplier Email</th>
						<th>Previous Due</th>
						<th>Supplier Address</th>
						<th><a href="supplier_add.php" id="example1" class="view btn btn-primary">Add Supplier</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysqli_query($conn, "SELECT * FROM  personinformation where type='supplier' order by id DESC");

					while ($info = mysqli_fetch_array($query))
					{
					?>
					<tr>
						<td><?php echo $info['name'];?></td>
						<td><?php echo $info['phone'];?></td>
						<td><?php echo $info['email'];?></td>
						<td><?php echo $info['due'];?></td>
						<td><?php echo $info['address'];?></td>
						<td>
							<span class="pull-right">
								<a title="View" href="supplier_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="supplier_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="supplier_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
