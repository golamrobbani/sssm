<?php
    include('pos.php');
    include('word.php');
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title> Invoice</title>

    <style>
    body{
        background: #e2e2e2
    }
    .invoice-box {
        width: 1120px;
        margin: auto;
        padding: 5px;

        font-size: 11px;
        line-height: 15px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
        float:left;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 0px;
        vertical-align: center;
        height: 20px;
    }

    /*.invoice-box table tr td:nth-child(2) {*/
    /*    text-align: right;*/
    /*}*/

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 30px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 10px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 10px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }

        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }

    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }

    .rtl table {
        text-align: right;
    }

    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    .half {
        width: 1000px;
        margin: 0 auto;
        background: white;
        padding: 10px;

    }

    table .list {
        /*border: 1px solid black;*/
    }
    table tr{
        border: 1px solid white;
    }
    .total{
       text-transform: capitalize;
    }

    @media print {

         tr.label{
        height:20px;
        width: 100%;
        background: #558cda !important;
        -webkit-print-color-adjust: exact;
    }
     body * { visibility: hidden; }
    .half * { visibility: visible; }
    .half { position: absolute; top: 40px; left: 30px; }
 }

    }
    </style>
</head>


<body>

    <?php
    $id=$_GET['inv'];
    $sql="SELECT * from purchases_return where id='$id'";
    $result=mysqli_query($conn, $sql);
    $count=mysqli_num_rows($result);
    if($count>0)
    {
    while($data=mysqli_fetch_array($result))
    {
        $name=$data['supplier'];
        $address=$data['address'];
        $contact=$data['contact'];
        $date=$data['date'];
        $gtotal=$data['gtotal'];
        $payable=$data['payable'];
        $tdiscount=$data['discount'];
        $paid=$data['paid'];
        $due=$data['due'];
        $cammount=$data['cammount'];
        $pmethod=$data['pmethod'];
        $pnumber=$data['pnumber'];
    }
    ?>
    <div class="invoice-box" id="print">
      <a href="#" style="font-size: 20px;" class="btn btn-warning"  onclick="window.print();">Print Invoice</a>
        <div class="half">
            <div style="width: 100%; text-align: center; justify-content: center; align-items: center;">

                        <img src="images/1575484846.png" style="
                        width: 595px;
                        height: 87px;
                        margin-bottom: 10px;
                        vertical-align: top;
                        ">
            </div>
            <div  style="    width: 100%;  display: inline-block;">

                <div  style="    width: 100%; float: left;  margin-right: 120px; text-align: center;">
                    <span style="font-size: 30px; font-weight: 400">demo address </span><br/>
                    <span style="font-size: 14px;">demo address2</span><br/>
                    <span style="font-size: 14px;">Mobile No 01759389686</span><br/>
                    <span style="font-size: 14px;     border-bottom: 2px solid black;"></span><br/><br/>
                    <span style="margin-top: 5px; font-size: 20px; font-weight: 600"></span><br/>

                </div>
            </div>

<div style="width: 100%;  display: inline-block;">
    <div style="width: 60%; float: left;margin-top: 13px;    border: 1px solid;  padding: 3px; margin-right: 10px; margin-bottom: 20px;">
        <p style="font-size: 16px; float: left;">Name:&nbsp;&nbsp;</p> <p style="    font-size: 16px; float: left;  width: 83%;  border-bottom: 1px solid;"><?php echo $name; ?></p>
        <p style="font-size: 16px; float: left;     margin-top: 0px;">Address:&nbsp;&nbsp;</p> <p style="    font-size: 16px; float: left;  width: 80%;  border-bottom: 1px solid;     margin-top: 0px;"><?php echo $address; ?></p>

    </div>
    <div style="width: 35%; float: right; margin-top: 13px; margin-bottom: 20px;  border: 1px solid;  padding: 3px; margin-right: 7px;">
                <p style="font-size: 16px; float: left;">Date:&nbsp;&nbsp;</p> <p style="    font-size: 16px; float: left;  width: 83%;  border-bottom: 1px solid;"> &nbsp;<?php echo date("d-m-Y", $date); ?></p>
        <p style="font-size: 16px; float: left;     margin-top: 0px;">Bill No:&nbsp;&nbsp;</p> <p style="    font-size: 16px; float: left;  width: 79%;  border-bottom: 1px solid;     margin-top: 0px;">#<?php echo $id; ?></p>


    </div>
</div>

<div style="width: 98%; height: 400px;   padding: 5px; border: 1px solid">

<div style="width: 100%; float: left;  padding-bottom: 5px;  border-bottom: 1px solid black; position: relative;">
    <div style="width: 8%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">Lot/ship No.</span></div>

    <div style="height: 400px; background: black; left: 9%; position: absolute;  z-index: 999; border-left: 1px solid black;"></div>

    <div style="width: 20%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">Description of Items</span></div>

    <span style="height: 400px;  border-left: 1px solid black; background: black; float: left; position: absolute;  z-index: 999;"></span>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">Size</span></div>

    <span style="height: 400px;  border-left: 1px solid black; background: black; float: left; position: absolute;  z-index: 999;"></span>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">Color</span></div>

    <span style="height: 400px;  border-left: 1px solid black; background: black; float: left; position: absolute;  z-index: 999;"></span>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">TTL Qty.</span></div>

    <span style="height: 400px;  border-left: 1px solid black; background: black; float: left; position: absolute;  z-index: 999;"></span>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">TTL Corton Qty.</span></div>

    <span style="height: 400px;  border-left: 1px solid black; background: black; float: left; position: absolute;  z-index: 999;"></span>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">Factory Price</span></div>

    <span style="height: 400px;  border-left: 1px solid black; background: black; float: left; position: absolute;  z-index: 999;"></span>

   <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">Total Price AFD</span></div>

    <span style="height: 400px;  border-left: 1px solid black; background: black; float: left; position: absolute;  z-index: 999;"></span>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 18px; font-weight: 500">Remarks</span></div>
</div>

<?php

$sql="SELECT * from purchases_return_product where sid='$id'";
$result=mysqli_query($conn, $sql);
$count=mysqli_num_rows($result);
$x = 0;

        while($info=mysqli_fetch_array($result))
        {
                $x += 1;
                $item_id=$info['item_id'];
                $name=$info['name'];
                $quty=$info['quty'];
                $sell=$info['sell'];
                $discount=$info['discount'];
                $total=$info['total'];
                $unit=$info['unit'];
                $per_unit=$info['per_unit'];
                $pcode=$info['pcode'];


                $pro="SELECT * from product_details where id='$item_id'";
                $pros=mysqli_query($conn, $pro);
                while($pinfo=mysqli_fetch_array($pros))
                {
                    $product_color = $pinfo['product_color'];
                    $product_size = $pinfo['product_size'];
                }

?>

<div style="width: 100%; float: left;  padding-bottom: 5px;  padding-top: 5px;  position: relative;">

    <div style="width: 8%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal;"><?php echo $x; ?></span></div>

    <div style="width: 20%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal"><?php echo $name; ?></span></div>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal"><?php echo $product_color; ?></span></div>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal"><?php echo $product_size; ?></span></div>


    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal"><?php echo $quty; ?></span></div>


    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal"><?php echo $quty; ?></span></div>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal">Tk <?php echo $sell; ?></span></div>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal">Tk <?php echo $total; ?></span></div>

    <div style="width: 10%; float: left; text-align: center;"><span style="font-size: 14px; font-weight: normal"><?php ?></span></div>
</div>
 <?php
     }
?>



</div>


              <table  cellpadding="0" cellspacing="0" style="margin-bottom: 20px;">
              <tr> <td style="width: 70%;
    float: left;
    margin-top: 5px;"><b style="font-size: 15px; display: inline-block;">In Words:</b> <span style=" font-size: 15px; border-bottom: 1px solid black;
    border-bottom-style: dotted;
    width: 86%;
    display: inline-block;
        text-transform: capitalize;
    padding-left: 5px;"><?php echo convert_number_to_words($payable); ?> Taka Only</span></td> <td style="width: 30%;; font-size: 15px; display: inline-block;"><b style="    float: left;
    margin-left: 50px;
    margin-top: 6px;">Total:</b> <b style="    width: 53%;
    float: right;
    margin-right: 5px;
    padding: 5px;
    border-left: 1px solid;
    border-right: 1px solid;
    border-bottom: 1px solid;">Tk <?php echo $payable; ?></b></td> </tr>
             </table>

              <table  cellpadding="0" cellspacing="0">
              <tr> <td><br/> <b style="border-top: 1px solid;">Reciver's Signature & Seal</b> </td>

              <td style="text-align: right;">
              <!-- <img src="http://bcis-bd.com/soft/images/signeture.png" style="height: 30px;width: 156px;"> -->
                            <br/> <b style="margin-right: 16px;"><b style="border-top: 1px solid;">Authorised Signature</b> </b>

              </td> </tr>
             </table>

        </div>

    </div>
    <?php }else{
        echo "<h1 style='text-align: center; padding: 50px; background: white; margin-left: 50px; margin-right: 50px;'>No sale information found!!!</h1>";
    } ?>

</body>



</html>
