<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Barcode Setting</div>
		<div class="panel">
			<!--View-->
			 <!-- content starts -->
			<div class="alert alert-info"><h3>Barcode Setup</h3></div>
				<?php
					if($_POST)
					{
						$row1 = $_POST['row1'];
						$row2 = $_POST['row2'];
						$row4 = $_POST['row4'];
						$row3 = $_POST['row3'];
						$row5 = $_POST['row5'];
						$req="update barcode_setup set row1='".$row1."', row2='".$row2."', row4='".$row4."', row3='".$row3."', row5='".$row5."'";
						if (mysqli_query($conn,$req))
						{
							echo"<script>location.href='barcode_setting.php?message=update'</script>";
						}
						else
						{
							echo"<script>location.href='barcode_setting.php?message=error'</script>";
						}
					}
				?>


				<?php
					if (!empty($_GET['message']) && $_GET['message'] == 'success') {
						echo '<div class="alert alert-success">' ;
						echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
						echo '<h4>Your Data Successfully Inserted</h4>';
						echo '</div>';
					}
					else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
						echo '<div class="alert alert-success">' ;
						echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
						echo '<h4>Your Data Successfully Updated</h4>';
						echo '</div>';
					}
					else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
						echo '<div class="alert alert-success">' ;
						echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
						echo '<h4>Your Data Successfully Deleted</h4>';
						echo '</div>';
					}
					else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
						echo '<div class="alert alert-success">' ;
						echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
						echo '<h4>Your Data Uploaded Error ! </h4>';
						echo '</div>';
					}
					else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
						echo '<div class="alert alert-success">' ;
						echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
						echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
						echo '</div>';
					}

				?>

				<?php
				$query = mysqli_query($conn,"SELECT * FROM barcode_setup");
				while ($info = mysqli_fetch_array($query))
				{?>
				<form action="" method="POST" class="form">

					<table width="500">
						<tr>
							<td>
									<label >Row 1</label>
							</td>
							<td><select name="row1">
									<option value="">please select</option>
									<option value="">please select</option>
									<option value="name" selected="selected" >Product Name</option>
									<option value="product_code" >Product Code</option>
									<option value="category" >Product Category</option>
									<option value="sale_price" >Retail Price</option>
									<option value="wholesale_price" >Wholesale Price</option>
									<option value="brand" >Brand Name</option>
									<option value="warranty" >Warranty</option>
									<option value="expire_date" >Expire Date</option>
								<select>
							</td>
						</tr>
						<tr>

							<td>
									<label> Row 2</label>
							</td>
							<td>
								<select name="row2">
									<option value="">please select</option>
									<option value="name"  >Product Name</option>
									<option value="product_code" >Product Code</option>
									<option value="category" >Product Category</option>
									<option value="sale_price" >Retail Price</option>
									<option value="wholesale_price" >Wholesale Price</option>
									<option value="brand" selected="selected">Brand Name</option>
									<option value="warranty" >Warranty</option>
						<option value="expire_date" >Expire Date</option>
								<select>
							</td>
						</tr>

						<tr>
							<td>
									<label>Row 3</label>

							</td>
							<td><select name="row3">
									<option value="">please select</option>
									<option value="name"  >Product Name</option>
									<option value="product_code" >Product Code</option>
									<option value="category" >Product Category</option>
									<option value="sale_price" selected="selected">Retail Price</option>
									<option value="wholesale_price" >Wholesale Price</option>
									<option value="brand" >Brand Name</option>
									<option value="warranty" >Warranty</option>
									<option value="expire_date" >Expire Date</option>
								<select>
						</tr>
						<tr>
							<td>
									<label>Row 4</label>

							</td>
							<td><select name="row4">
									<option value="">please select</option>
									<option value="name"  >Product Name</option>
									<option value="product_code" >Product Code</option>
									<option value="category" >Product Category</option>
									<option value="sale_price" selected="selected">Retail Price</option>
									<option value="wholesale_price" >Wholesale Price</option>
									<option value="brand" >Brand Name</option>
									<option value="warranty" >Warranty</option>
									<option value="expire_date" >Expire Date</option>
								<select>
						</tr>
						<tr>
							<td>
									<label>Row 5</label>

							</td>
							<td><select name="row5">
									<option value="">please select</option>
									<option value="name"  >Product Name</option>
									<option value="product_code" >Product Code</option>
									<option value="category" >Product Category</option>
									<option value="sale_price" >Retail Price</option>
									<option value="wholesale_price" selected="selected">Wholesale Price</option>
									<option value="brand" >Brand Name</option>
									<option value="warranty" >Warranty</option>
									<option value="expire_date" >Expire Date</option>
								<select>
						</tr>
						<tr>
							<td>

							</td>
							<td>
								<input type="submit" class="view btn-success" name="submit" value="Save"/>
							</td>

						</tr>
					</table>

				</form>
				<?php }?>
		</div>
	</div>
<?php include 'footer.php';?>
