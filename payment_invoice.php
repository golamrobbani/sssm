<?php
include("data.php");
 ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title> Invoice</title>
<link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">

    <style>
    body{
    	background: #e2e2e2
    }
    .invoice-box {
        width: 1120px;
        margin: auto;
        padding: 5px;

        font-size: 11px;
        line-height: 15px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
        float:left;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 0px;
        vertical-align: center;
        height: 20px;
    }

    /*.invoice-box table tr td:nth-child(2) {*/
    /*    text-align: right;*/
    /*}*/

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 30px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 10px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 10px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }

        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }

    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }

    .rtl table {
        text-align: right;
    }

    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    .half {
        width: 1020px;
        margin: 0 auto;
        background: white;
        padding: 10px;

    }

    table .list {
        /*border: 1px solid black;*/
    }
    table tr{
    	border: 1px solid white;
    }
    .total{
       text-transform: capitalize;
    }

    @media print {

         tr.label{
        height:20px;
        width: 100%;
        background: #558cda !important;
        -webkit-print-color-adjust: exact;
    }
     body * { visibility: hidden; }
    .half * { visibility: visible; }
    .half { position: absolute; top: 40px; left: 30px; }
 }

    }
    </style>
</head>

<?php
$id = $_GET['id'];
$q = mysqli_query($conn, "SELECT * FROM  payment where  id='$id'");
while ($info = mysqli_fetch_array($q))
{

 ?>


<body>

    <div class="invoice-box" id="print">
      <a href="#" style="font-size: 20px;" class="btn btn-warning"  onclick="window.print();">Print Invoice</a>
        <div class="half">
            <div style="width: 100%; text-align: center; justify-content: center; align-items: center;">

                        <img src="images/1575484846.png" style="
                        width: 595px;
                        height: 87px;
                        margin-bottom: 10px;
                        vertical-align: top;
                        ">
            </div>
            <div  style=" width: 100%;  display: inline-block;">

                <div  style="    width: 100%; float: left;  margin-right: 120px; text-align: center;">
                    <span style="font-size: 30px; font-weight: 400">demo address </span><br/>
                    <span style="font-size: 14px;">demo address2</span><br/>
                    <span style="font-size: 14px;">Mobile No 01759389686</span><br/>
                    <span style="font-size: 14px;     border-bottom: 2px solid black;"></span><br/><br/>
                    <span style="margin-top: 5px; font-size: 20px; font-weight: 600">Cash Memo</span><br/>

                </div>
            </div>

            <div style="width: 100%;  display: inline-block;">
                <div style="width: 60%; float: left;margin-top: 13px;    border: 1px solid;  padding: 3px; margin-right: 10px; margin-bottom: 20px;">
                    <p style="font-size: 16px; float: left;">Name:&nbsp;&nbsp;</p> <p style="    font-size: 16px; float: left;  width: 83%;  border-bottom: 1px solid;"><?php echo $info['name']; ?></p>

                </div>
                <div style="width: 35%; float: right; margin-top: 13px; margin-bottom: 20px;  border: 1px solid;  padding: 3px; margin-right: 7px;">
                            <p style="font-size: 16px; float: left;">Date:&nbsp;&nbsp;</p> <p style="    font-size: 16px; float: left;  width: 83%;  border-bottom: 1px solid;"> &nbsp;<?php echo date("d-m-Y", $info['date']); ?></p>


                </div>
            </div>


        	<div style="height: 400px;">
        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">Recive With Thanks From:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 78%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; "><?php echo $info['name']; ?></p>

        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">Invoice No:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 40%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; ">#<?php echo $info['order_id']; ?></p>
        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">&nbsp;Bill No:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 42%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; ">#<?php echo $info['id']; ?></p>

        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">The sum of taka:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 85%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; ">Tk <?php echo $info['payment']; ?></p>

        			<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">By Cash / Chaque No:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 40%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; "><?php echo $info['payment_type']=='cash'? "Cash" : $info['chaque_no'] ?></p>
        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">&nbsp;Dated:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 35%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; "><?php echo date("d-m-Y", $info['dated']) ?></p>

        		        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">Bank:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 45%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; "><?php echo $info['bank_name']; ?></p>
        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">&nbsp;Branch:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 41%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; "><?php echo $info['branch_name']; ?></p>

        		        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">Against the bill / Order no:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 27%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; ">#<?php echo $info['order_id']; ?></p>
        		<p style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">&nbsp;Dated:&nbsp;&nbsp;</p> <p style="    font-size: 20px; float: left;  width: 44%;  border-bottom: 2px solid; border-bottom-style: dotted; padding-bottom: 3px; "><?php echo date("d-m-Y", $info['date']) ?></p>

        	</div>




              <table  cellpadding="0" cellspacing="0">
              <tr> <td><span  style="font-size: 20px; float: left;  font-family: 'Charm', cursive;">Taka</span>  <span  style="    font-size: 20px; float: left; width: 166px;
    height: 20px;  border: 1px solid; padding: 5px; text-align: center; justify-content: center;  margin-top: -8px; margin-left: 10px;">Tk <?php echo $info['payment']; ?></span> </td>

              <td style="text-align: right;">
              <br/> <b style="margin-right: 16px;"><b style="border-top: 1px solid;">Authorised Signature</b> </b>

              </td> </tr>
             </table>


        </div>

    </div>
</body>

<?php } ?>

</html>
