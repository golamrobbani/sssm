<div class="modalform">
    Add a Personal Loan<hr/>
    <form action="" method="POST" class="form">
    	<table class="tab">
    	<tr>
    	<td align="right">Date</td>
    	<td><input type="text" name="date" id="date" class="datepick" value="<?php echo date('d/m/Y');?>" /></td>
    	</tr>
        <tr>
        <td align="right">From</td>
        <td><input type="text" name="name" id="name" ></td>
        </tr>
        <tr>
        <td align="right">Amount </td>
        <td><input type="number" id="q1" name="amount" oninput="calculate()"></td>
        </tr>
         <tr>
        <td align="right">Interest %</td>
        <td><input type="number" id="q2" name="interest" oninput="calculate()"></td>
        </tr>
         <tr class="hide">
        <td align="right">Payable Amount </td>
        <td><input type="number" id="q3" name="payable_amount" ></td>
        </tr>
        <tr>
        <td align="right">Return Date </td>
        <td><input type="text" name="return_date" placeholder="dd/mm/yy" id="return_date" class="datepick" ></td>
        </tr>
    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right"> Comments</td>
    	<td><textarea  name="comments" id="comments" rows="5" cols="20"></textarea></td>
    	</tr>
		<tr class="hide">
        <td align="right">Type </td>
        <td><input type="text" name="type" value="personal" id="type"></td>
        </tr>
    	<tr>
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input class="view btn-success" type="submit" value="Submit"></td>
    	</tr>
    	</table>
    </form>
</div>