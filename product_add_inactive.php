<form action="product_add_inactive_action.php" method="POST" enctype="multipart/form-data" class="form">
    <table class="tab">
    	<tr> 
    	<td align="right">Product Name</td>
    	<td><input type="text" id="name" name="name" placeholder="Enter Product Name" required></td>
    	</tr>
        <tr>
        <td align="right">Product Code</td>
        <td><input type="text" id="product_code" name="product_code" placeholder="Enter Product code"></td>
        </tr>
        <tr>
        <td align="right">Product Category</td>
        <td>
            <input class="form-control form" id="category" name="category" placeholder="Enter Category" type="text" required>
        </td>
        </tr>
         <tr>
        <td align="right">Purchase Cost</td>
        <td><input class="form-control form" id="purchase_cost" name="purchase_cost" placeholder="Enter Purchase cost" type="text" required></td>
        </tr>
        <tr>
        <td align="right">Retail Sell Price</td>
        <td><input class="form-control form" id="sale_price" name="sale_price" placeholder="Enter Sale Price" type="text" required></td>
        </tr>
        <tr>
        <td align="right">Wholesale Price</td>
        <td><input class="form-control form" id="wholesale_price" name="wholesale_price" placeholder="Enter Wholesale Price" type="text"></td>
        </tr>
        <tr>
        <td align="right">Stock</td>
        <td><input class="form-control form" id="quantity" name="quantity" placeholder="Enter Quantity" type="text"></td>
        </tr>
		<tr>
        <td align="right">Minimum Stock Quantity</td>
        <td><input class="form-control form" id="minquantity" name="minquantity" placeholder="Enter  Quantity" type="text"></td>
        </tr>
       
        <tr>
        <td align="right">Unit type</td>
        <td>
            <select style="" class="form-control form" id="unit_type" name="unit_type">
               <option value="">Please select</option><option value="pcs">pcs</option><option value="winer">winer</option><option value="pair">pair</option><option value="Meter">Meter</option><option value="Cs">Cs</option>            </select>
        </td>
        </tr>
		<tr class="hide">
        <td align="right">Status</td>
        <td><input class="form-control form" id="status" name="status" value="inactive" type="text">
        </td>
        </tr>
         <tr>
        <td align="right">Brand Name</td>
        <td>
            <input class="form-control form" id="brand" name="brand" placeholder="Enter Brand" type="text">
        </td>
        </tr>
        <tr>
        <td valign="top" style="padding-top:20px;" align="right">Description</td>
        <td><textarea rows="5" cols="20" id="description" name="description" type="text"></textarea></td>
        </tr>
    	<tr>
    	<td align="right">Vat</td>
    	<td><input class="form-control form" id="vat" name="vat" type="text"></td>
    	</tr>
    	<tr>
    	<td align="right">Warranty</td>
    	<td><input class="form-control form" id="warranty" name="warranty" type="text"></td>
    	</tr>
        <tr>
        <td align="right">Expiery Date</td>
        <td><input class="form-control form datepick" id="expire_date" name="expire_date" placeholder="dd/mm/yyyy" type="text"></td>
        </tr>
		<tr class="">
        <td align="right">Product Add Date</td>
        <td><input class="form-control form datepick" id="product_add_date" name="product_add_date" value="<?php echo date('d/m/Y');?>" type="text"></td>
        </tr>
    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right">Comments</td>
    	<td><textarea  id="comments" name="comments" rows="5" cols="20" type="text"></textarea></td>
    	</tr>
		<tr>
    	<td valign="top" align="right">Image</td>
    	<td><input type="file" name="image"></td>
    	</tr>
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input type="submit" class="view btn-success" value="Save"></td>
    	</tr>
        
    </table>
</form>