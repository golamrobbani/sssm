<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Payment Report</div>
		<div class="panel">
		<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
			 	</table>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id" class="display table table-bordered">
					<thead>
						<tr>
						<th>ID No.</th>
						<th>Date</th>
						<th>Supplier Name</th>
						<th>Payment Amount</th>
						<th>Due Amount</th>
						<th>Payment Type</th> 
						<th>Payment By</th>
						<th>Payment Note</th>
						<th></th>
						</tr>
					</thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{
						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);


					$purchase = mysqli_query($conn, "SELECT * FROM  payment where date between '$from' and '$to' order by id asc");
						$req = mysqli_query($conn, "SELECT sum(payment) FROM payment where date between '$from' and '$to'");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(payment)'];

					}
					while ($info = mysqli_fetch_array($purchase))
					{

						$cid = $info['payment_customer'];
						$cus = mysqli_query($conn, "SELECT * FROM   personinformation where type='customer' and id =$cid");
						while ($cuss = mysqli_fetch_array($cus))
						{
					 	 $cname=$cuss['name'];
						}


					?>
					<tr>
						<td>#<?php echo $info['id'];?></td>
						<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td><?php echo $info['name'];?></td>
						<td>Tk <?php echo number_format($info['payment'],2);?></td>
						<td>Tk <?php echo number_format($info['due'] - $info['payment'],2);?></td>	
						<td><?php echo $info['payment_type'];?></td>
						<td><?php echo $info['payment_customer'] == 0 ? "Admin" : $cname ;?></td>
						<td><?php echo $info['note'];?></td>

						<td>
							<span class="pull-right">
								 <a title="Recive" href="payment_edit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-info">Edit</a>

								<a title="Invoice"  target="_blank" href="payment_invoice.php?id=<?php echo $info['id'];?>"  class="view btn-success">Invoice</a>

								<a title="Delete" href="report_payment_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
								</span>
							</span>
						</td>
					</tr>
					<?php	 } }
					else
					{
					$purchase = mysqli_query($conn, "SELECT * FROM  payment order by id asc");
						$req = mysqli_query($conn, "SELECT sum(payment) FROM payment order by id asc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(payment)'];

					}
					while ($info = mysqli_fetch_array($purchase))
					{
						$cid = $info['payment_customer'];
						$cus = mysqli_query($conn, "SELECT * FROM   personinformation where type='customer' and id =$cid");
						while ($cuss = mysqli_fetch_array($cus))
							{
						 	 $cname=$cuss['name'];
							}

					?>
					<tr>
						<td>#<?php echo $info['id'];?></td>
						<td><?php echo date("d-m-Y", $info['date']); ?></td> 
						<td><?php echo $info['name'];?></td>
						<td>Tk <?php echo number_format($info['payment'],2);?></td>		
						<td>Tk <?php echo number_format($info['due'] - $info['payment'],2);?></td>	
						<td><?php echo $info['payment_type'];?></td>
						<td><?php echo $info['payment_customer'] == 0 ? "Admin" : $cname ;?></td>
						<td><?php echo $info['note'];?></td>


						<td>
							<span class="pull-right">
								 <a title="Recive" href="payment_edit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-info">Edit</a>
								<a title="Invoice"  target="_blank" href="payment_invoice.php?id=<?php echo $info['id'];?>"  class="view btn-success">Invoice</a>

							 	<a title="Delete" href="report_payment_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
								</span>
							</span>
						</td>
					</tr>
					<?php } }?>
					</tbody>
				</table>
						<br />
	<br />
	<table>
		<tr>
			<th align="left">Total Payment : </th>
			<td align="right"><?php echo  number_format($payable,2); ?> Tk </td>
		</tr>

	</table>

			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
