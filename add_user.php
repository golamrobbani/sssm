<script type="text/javascript">
function checkproductdetails(val){
	
	$.POST('check_vat.php', {productid: val},
	function (data) {
		
		$("#vat_name").val(data.vat_name);
		$("#amount").val(data.amount);		
		$("#comments").val(data.comments);
		$("#status").val(data.status);
	}, 'json');
	
	$('#hidden_field').html('<input type="hidden" name="id" value='+val+' />');
}
function emptyform(){
		$("#vat_name").val('');
		$("#amount").val('');		
		$("#comments").val('');
		$("#status").val('');
		$("#hidden_field").html('');

}
function branch_view(val){
	$('#branch_view tr').remove(); 
	$.POST('view_user.php', {productid: val},
	function (data) {		
		$(data).appendTo('#branch_view');		
	});
	
	//$('#hidden_field').html('<input type="hidden" name="id" value='+val+' />');
}

$(function () {

	$("#employee_name").autocomplete("employee1.php", {
		width: 180,
		autoFill: true,
		selectFirst: true
	});
});


function toggle(source) {
  checkboxes = document.getElementsByName('access[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}


</script>
    	<form action="" method="POST" class="form">
    	<table class="tab">
    	<tr>
    	<td align="right">Employee Name</td>
    	<td><input type="text" name="employee_name" id="employee_name" placeholder="Enter employee name" required></td>
    	</tr>    	
    	<tr>
    	<td align="right">User Nane</td>
    	<td>
    		<input type="text" name="username" id="username" placeholder="Enter username" required>
    	</td>
    	</tr>
		<tr>
    	<td align="right">Password</td>
    	<td>
    		<input type="password" name="password" id="password" placeholder="Enter password" required>
    	</td>
    	</tr>
	<tr>
    	<td align="right">User Type</td>
    	<td>
    		<select name="type">
    			<option value="mid-admin">Mid-Admin</option>
    			<option value="user">User</option>
    		</select>
    	</td>
    	</tr>

		<tr>
    	<td align="right" valign="top" style="padding-top:40px;">Access rights</td>
    	<td>
    		<table>

    			<tr>
					<td>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value=""  onclick="toggle(this)" />SELECT ALL</td>
							</tr>
						</table>
					</td>					
				</tr>
				<tr>
					<td><h2>Dashbord</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/dashboard.php" />Dashbord</td>
 
							</tr>
						</table>
					</td>					
				</tr>
				<tr>
					<td><h2>Stock</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/stock.php" />Stock</td>
								<td><input type="checkbox" name="access[]" value="/pos/low.php" />Low Stock</td>
								<td><input type="checkbox" name="access[]" value="/pos/damage.php" />Damage Stock</td>
							</tr>
						</table>
					</td>					
				</tr>
				<tr>
					<td><h2>Purchase</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/purchase.php" />Purchase</td>
								<td><input type="checkbox" name="access[]" value="/pos/purchase_return.php" />Purchase Return</td>
								
							</tr>
						</table>
					</td>					
				</tr>
				<tr>
					<td><h2>Sales</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/sale.php" />Sale</td>
								<td><input type="checkbox" name="access[]" value="/pos/sale_return.php" />Sale Return</td>
								<td><input type="checkbox" name="access[]" value="/pos/invoice_free.php" />Chalan</td>
								
							</tr>
						</table>
					</td>					
				</tr>
				<tr>
					<td><h2>Accounts</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/earnhead.php" />Earning Head</td>
								<td><input type="checkbox" name="access[]" value="/pos/earning.php" />Earnings</td>
								<td><input type="checkbox" name="access[]" value="/pos/expensehead.php" />Expense Head</td>
								<td><input type="checkbox" name="access[]" value="/pos/expense.php" />Expense</td>
								
							</tr>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/bank.php" />Bank</td>
								<td><input type="checkbox" name="access[]" value="/pos/investment.php" />Investment</td>
								<td><input type="checkbox" name="access[]" value="/pos/payment.php" />Payment</td>
								<td><input type="checkbox" name="access[]" value="/pos/outstanding.php" />Outstanding</td>
								
							</tr>
						</table>
					</td>					
				</tr>
				<tr>
					<td><h2>Loan</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/personal_loan.php" />Personal Loan</td>
								<td><input type="checkbox" name="access[]" value="/pos/bank_loan.php" />Bank Loan</td>
								<td><input type="checkbox" name="access[]" value="/pos/paypersonalloan.php" />Pay Personal Loan</td>
								<td><input type="checkbox" name="access[]" value="/pos/paybankloan.php" />Pay Bank Loan</td>								
							</tr>
						</table>
					</td>					
				</tr>
				<tr>
					<td><h2>Cash Transfer</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/c2b.php" />Cash to Bank</td>
								<td><input type="checkbox" name="access[]" value="/pos/b2c.php" />Bank To Cash</td>
							</tr>
						</table>
					</td>					
				</tr>
				<tr>
					<td><h2>Salary</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/salary_payment.php" />Salary</td>
								
							</tr>
						</table>
					</td>					
				</tr>	
			
				<tr>
					<td><h2>Reports</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/purchase_report.php" />Purchase Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_purchase_return.php" />Purchase Return Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_sales.php" />Sales Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_sales_return.php" />Sales Return Report</td>
								
							</tr>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/report_receive.php" />Receive Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_receive_due.php" />Receive Due Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_payment.php" />Payment Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_payment_due.php" />Payment Due Report</td>
								
							</tr>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/report_customer.php" />Customer Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_supplier.php" />Supplier Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_employee.php" />Employee Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/salary_report.php" />Salary Report</td>
								
							</tr>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/report_expense.php" />Expense Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_earn.php" />Earning Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/low_stock_report.php" />Low Stock Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/stock_report.php" />Stock Report</td>
								
							</tr>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/profit_lose.php" />Profit/Loss Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/report_user_log.php" />User Log Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/customer_ledger.php" />Customer Ledger Report</td>
								<td><input type="checkbox" name="access[]" value="/pos/supplier_ledger.php" />Supplier Ledger Report</td>
								
							</tr>
						</table>
					</td>					
				</tr>				
				<tr>
					<td><h2>Configuration</h2>
						<table>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/shop.php" />Shop</td>
								<td><input type="checkbox" name="access[]" value="/pos/branches.php" />Branches</td>
								<td><input type="checkbox" name="access[]" value="/pos/customer.php" />Customer</td>
								<td><input type="checkbox" name="access[]" value="/pos/supplier.php" />Supplier</td>
								
							</tr>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/investor.php" />Investor</td>
								<td><input type="checkbox" name="access[]" value="/pos/employee.php" />Employee</td>
								<td><input type="checkbox" name="access[]" value="/pos/product.php" />Product</td>
								<td><input type="checkbox" name="access[]" value="/pos/inactive.php" />Inactive Product</td>
								
							</tr>
							<tr>
								<td><input type="checkbox" name="access[]" value="/pos/category.php" />Category</td>
								<td><input type="checkbox" name="access[]" value="/pos/brand.php" />Brand</td>
								<td><input type="checkbox" name="access[]" value="/pos/unit.php," />Unit</td>
								<td><input type="checkbox" name="access[]" value="/pos/dynamic_field.php" />Dynamic Field</td>
								
							</tr>
							<tr>
								
								<td><input type="checkbox" name="access[]" value="/pos/barcode_setting.php" />Barcode Setting</td>
								<td><input type="checkbox" name="access[]" value="/pos/barcode.php" />Multiple Barcode</td>
								<td><input type="checkbox" name="access[]" value="/pos/users.php" />Users</td>
								<td><input type="checkbox" name="access[]" value="/pos/backup.php,online.php" />Data Manage</td>
								
							</tr>
						</table>
					</td>					
				</tr>
				
			</table>
    	</td>
    	</tr>

    	<tr>
    	
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input type="submit" name="submit" value="Go"></td>
    	</tr>
    	</table>
    	</form>
