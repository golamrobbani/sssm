<?php include 'header.php';?>

	<div class="area">
		<div class="panel-head">Bank  Details</div>
		<div class="panel">
			<!--View-->

	<?php
				if($_POST)
				{
					$date = date('d-m-Y');
					$date = strtotime($date);
					$name = $_POST['name'];
					$ptype = $_POST['ptype'];
					$bname = $_POST['bname'];
					$aname = $_POST['aname'];
					$anumber = $_POST['anumber'];
					$comments = $_POST['comments'];
					$req="INSERT INTO bankinformation (date,person, person_type, bankname, accountname,accountnumber,comments)  VALUES ('$date','$name', '$ptype', '$bname', '$aname', '$anumber', '$comments')";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='bank.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='bank.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Person Name</th>
						<th>Person Type</th>
						<th>Bank Name</th>
						<th>Account Name</th>
						<th>Account Number</th>
						<!--<th>Current Amount</th>-->
					<th><a href="bank_add.php" id="example1" class="view btn btn-primary">Add Bank Information</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$querys = mysqli_query($conn, "SELECT * FROM  bankinformation");
					while ($bs = mysqli_fetch_array($querys))
					{ 
						$bid = $bs['id'];
 

					?>
					<tr>
						<td><?php echo $bs['person'];?></td>
						<td><?php echo $bs['person_type'];?></td>
						<td><?php echo $bs['bankname'];?></td>
						<td><?php echo $bs['accountname'];?></td>
						<td><?php echo $bs['accountnumber'];?></td>
						<!--<td>Tk <?php echo $mid_bank;?></td>-->
						<td>
							<span class="pull-right">
									<a title="View" href="bank_view.php?id=<?php echo $bs['id'];?>" id="example1" class="view btn-primary">View</a>
									<a title="Edit" href="bank_eidit.php?id=<?php echo $bs['id'];?>" id="example1" class="view btn-primary">Edit</a>
										<a title="Delete" href="bank_delete.php?id=<?php echo $bs['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a></span>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
