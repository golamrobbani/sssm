<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Reports</div>
		<div class="panel">
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">
					
						<tr>
							<td width="2%">From</td>
							
							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>
							
							<td width="2%">To</td>
							
							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>
							
							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
						<a onclick="" href="generate_profit_lose.php?from=&to=" class="btn-add btn-warning pull-right">Print Report</a>
				</table>
				</form>
				<div class="table_data">
				  <h2>Profit/Lose Report</h2>
					<table border=1 width="940px" class="tab">
					<thead>
						<tr>				     			
							<th>Total Earn</th>
							<th>Total Expense</th>
							<th>Profit/Lose</th>	
						</tr>
					</thead>
					<tbody>
					
					<tr>					<?php
					if(isset($_GET['Submit']))
					{
						$from=$_GET['from'];
						$to=$_GET['to'];
						
						
						$earning = mysql_query("SELECT SUM(amount) FROM  earning where date between '$from' and '$to' ");
						while ($earning_info = mysql_fetch_array($earning)) 
						{
						$total_earning= $earning_info['SUM(amount)'];	
						?>
						<td align="center"><?php echo $total_earning;?></td>
						<?php }?>
						
						<?php
						$expense = mysql_query("SELECT SUM(amount) FROM  expense where date between '$from' and '$to' ");
						while ($expense_info = mysql_fetch_array($expense)) 
						{
						$total_expense= $expense_info['SUM(amount)'];	
						?>
						<td align="center"><?php echo $total_expense;?></td>
						<?php }?>
						<?php 
						$profit_lose = $total_earning-$total_expense;
						?>
						<td align="center"><?php echo $profit_lose;?></td>
						
						<?php
					
					}
					else
					{
					?>
						<?php
						$earning = mysql_query("SELECT SUM(amount) FROM  earning");
						while ($earning_info = mysql_fetch_array($earning)) 
						{
						$total_earning= $earning_info['SUM(amount)'];	
						?>
						<td align="center"><?php echo $total_earning;?></td>
						<?php }?>
						
						<?php
						$expense = mysql_query("SELECT SUM(amount) FROM  expense");
						while ($expense_info = mysql_fetch_array($expense)) 
						{
						$total_expense= $expense_info['SUM(amount)'];	
						?>
						<td align="center"><?php echo $total_expense;?></td>
						<?php }?>
						<?php 
						$profit_lose = $total_earning-$total_expense;
						?>
						<td align="center"><?php echo $profit_lose;?></td>
						
					<?php } ?>
					</tr>
					</tbody>
				</table>
			   
			   
			   <h2>Sale</h2>
					<table border=1 width="940px" class="tab">
					<thead>
						
						<tr>
							<th>Sale</th>            				
							<th>Receive</th>
							<th>Due</th>
						</tr>
					</thead>
					
					<thead>
						<tr>
							<th align="center">Total</th> 
										<?php
					if(isset($_GET['Submit']))
					{
						$from=$_GET['from'];
						$to=$_GET['to'];
						$receive_payment = mysql_query("SELECT SUM(paid) FROM  sales where date between '$from' and '$to' ");
							while ($receive_payment_info = mysql_fetch_array($receive_payment)) 
							{
							$total_receive= $receive_payment_info['SUM(paid)'];	
							?>
							<th align="center"><?php echo round($total_receive);?></th>
							<?php }?>
							
							<?php
							$payable_payment = mysql_query("SELECT SUM(due) FROM  sales where date between '$from' and '$to' ");
							while ($payable_payment_info = mysql_fetch_array($payable_payment)) 
							{
							$total_payable= $payable_payment_info['SUM(due)'];	
							?>
							<th align="center"><?php echo round($total_payable);?></th>
					<?php }
						
					}
					else{
							$receive_payment = mysql_query("SELECT SUM(paid) FROM  sales");
							while ($receive_payment_info = mysql_fetch_array($receive_payment)) 
							{
							$total_receive= $receive_payment_info['SUM(paid)'];	
							?>
							<th align="center"><?php echo round($total_receive);?></th>
							<?php }?>
							
							<?php
							$payable_payment = mysql_query("SELECT SUM(due) FROM  sales");
							while ($payable_payment_info = mysql_fetch_array($payable_payment)) 
							{
							$total_payable= $payable_payment_info['SUM(due)'];	
							?>
							<th align="center"><?php echo round($total_payable);?></th>
					<?php } }?>
							
						</tr>
					</thead>
				</table>
				
			   <h2>Purchase</h2>
					<table border=1 width="940px" class="tab">
					<thead>
						
						<tr>
							<th>Purchase</th>            				
							<th>Payment</th>
							<th>Due</th>
						</tr>
					</thead>
				
					<thead>
						<tr>
							<th align="center">Total</th> 
							
							<?php	if(isset($_GET['Submit']))
					{
						$from=$_GET['from'];
						$to=$_GET['to'];
							$receive_payment = mysql_query("SELECT SUM(paid) FROM  purchases where date between '$from' and '$to' ");
							while ($receive_payment_info = mysql_fetch_array($receive_payment)) 
							{
							$total_receive= $receive_payment_info['SUM(paid)'];	
							?>
							<th align="center"><?php echo round($total_receive);?></th>
							<?php }?>
							
							<?php
							$payable_payment = mysql_query("SELECT SUM(due) FROM  purchases where date between '$from' and '$to' ");
							while ($payable_payment_info = mysql_fetch_array($payable_payment)) 
							{
							$total_payable= $payable_payment_info['SUM(due)'];	
							?>
							<th align="center"><?php echo round($total_payable);?></th>
					<?php }
					}
					else
					{
							$receive_payment = mysql_query("SELECT SUM(paid) FROM  purchases");
							while ($receive_payment_info = mysql_fetch_array($receive_payment)) 
							{
							$total_receive= $receive_payment_info['SUM(paid)'];	
							?>
							<th align="center"><?php echo round($total_receive);?></th>
							<?php }?>
							
							<?php
							$payable_payment = mysql_query("SELECT SUM(due) FROM  purchases");
							while ($payable_payment_info = mysql_fetch_array($payable_payment)) 
							{
							$total_payable= $payable_payment_info['SUM(due)'];	
							?>
							<th align="center"><?php echo round($total_payable);?></th>
					<?php } }?>
							
						</tr>
					</thead>
				</table>

				   
			   </div>
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
