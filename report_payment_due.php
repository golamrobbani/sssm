<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Payment Report</div>
		<div class="panel">
		<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
			 	</table>
				</form>
				<div class="table_data" id="mydiv">

								<table id="table_id" class="display table table-bordered">
									<thead>
										<tr>
											<th>Order No.</th>
											<th>Date</th>
											<th>Supplier Name</th>
											<th>Payable Amount</th>
											<th>Paid Amount</th>
											<th>Due Amount</th>

												<th>Payment Now </th> 
										</tr>
									</thead>
									<tbody>
										<?php
										$query = mysqli_query($conn, "SELECT * FROM  purchases where due>0 order by id asc");
										while ($info = mysqli_fetch_array($query))
										{
											$sid = $info['supplier_id'];
											$id = $info['id'];
											$due = $info['due'];
										$q = mysqli_query($conn, "SELECT sum(payment) FROM  payment where supplier_id='$sid' and order_id='$id'");
										while ($in = mysqli_fetch_array($q))
										{
											$totalPaid = $in['sum(payment)'];
										}

										if($due > $totalPaid){



											?>
										<tr>
											<td>#<?php echo $info['id'];?></td>
											<td><?php echo date("d-m-Y", $info['date']); ?></td>
											<td><?php echo $info['supplier'];?></td>
											<td>Tk <?php echo number_format($info['payable'],2);?></td>
											<td>Tk <?php echo number_format($info['paid'] + $totalPaid,2);?></td>
											<td>Tk <?php echo number_format($info['due'] - $totalPaid,2);?></td>

											<td>
												<span class="pull-right">
												  <a title="Payment" href="payment_now.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Payment Now</a>
													</span>
												</span>
											</td>
										</tr>
									<?php } }?>
									</tbody>
								</table>
							<br />

			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
