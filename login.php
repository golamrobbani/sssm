<?php include 'config.php';?>
<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset="UTF-8" />
		<title>:: SSSM ::</title>
		<link rel="shortcut icon" href="images/sssm.png" />
		<link rel="apple-touch-icon-precomposed" href="images/sssm.png" />
		<link rel="stylesheet" type="text/css" href="css/login.css">
	</head>

	<body>
		<div class="loginlogo"><center><img style="width: 100%" src="images/<?php echo $site_logo; ?>"/></div>
		<form  class="form-horizontal" action="action-log.php" name="login" method="POST">
			<h1>SSSM</h1>
			<?php
			if (!empty($_GET['error']))
			{
				if ($_GET['error'] == 1) {
					echo '<h3 align="center" style="color:#ff0000;">Please Enter Username or Passowrd!</h3>';
				} else if ($_GET['error'] == 2) {
					echo '<h3 align="center" style="color:#ff0000;">Username does not match!</h3>';
				} else if ($_GET['error'] == 3) {
					echo '<h3 align="center" style="color:#ff0000;">Password does not match!</h3>';
				} else if ($_GET['error'] == 4) {
					echo '<h3 align="center" style="color:#ff0000;">Username or Password Invalid!</h3>';
				}
			}
			?>
			<div class="inset">
			<p>
				<label for="email">USERNAME</label>
				<input type="text" name="username" id="username">
				</p>
				<p>
				<label for="password">PASSWORD</label>
				<input type="password" name="password" id="password">
			</p>
			</div>
			<p class="p-container">
			<!-- <span><a  style="color:#fff; text-decoration:none;" href="">Forgot password ?<a></span> -->
			<input type="submit" name="login" id="go" value="Login">
			</p>
		</form>
		<div class="loginfooter">
			&copy; <?php echo date('Y');?> SSSM<br/>
			Developed By: <a href="#" target="_blunck">Project Team</a>
		</div>
	</body>
</html>
