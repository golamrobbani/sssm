<?php
include 'pos.php';
?>
<?php
$query = mysqli_query($conn, "SELECT * FROM earning where id='$_GET[id]'");
while ($info = mysqli_fetch_array($query))
{?>
<form action="earning_edit_action.php?id=<?php echo $info['id'];?>" method="POST" class="form">
    <table class="tab">
    	<tr>
    	<td align="right">Date</td>
    	<td><input type="date" name="date" id="date" value="<?php echo date("d/m/Y", $info['date']); ?>" required></td>
    	</tr>
		<tr>
    	<td align="right">Earning Head Name</td>
    	<td>
		<select name="earning_head" id="earning_head"required>
			<option value="<?php echo $info['earning_head'];?>"><?php echo $info['earning_head'];?></option>
			<?php
				$data=mysqli_query($conn, "SELECT * FROM earning_head");
				while($data_info=mysqli_fetch_array($data))
				{
				$head_name= $data_info['head'];
				?>
				<option value="<?php echo $head_name;?>"><?php echo $head_name;?></option>
			<?php }?>
		</select>
		</td>
    	</tr>
		<tr>
    	<td align="right">Earning Amount</td>
    	<td><input  type="number"  step="0.01"  name="amount" id="amount" value="<?php echo $info['amount'];?>"required></td>
    	</tr>
    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right">Comments</td>
    	<td><textarea placeholder="Enter Comments" name="comments" id="comments" rows="5" cols="20"><?php echo $info['comments'];?></textarea></td>
    	</tr>
    	<tr>
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input type="submit" class="view btn-success" value="Update"></td>
    	</tr>
    </table>
</form>
<?php }?>
