
			<div style="margin:10px;auto">
				<p style="text-align:center;margin-bottom:-15px;font-size:13px;color:#333;">&copy; <?php echo date('Y');?> Developed By: <a class="link-footer" href="#" target="_blunck">Project Team</a></p><br/>
			</div>

		</div>



		<style>
		caption{
			position: absolute;
			right: 0;
			top: 34px;
			z-index: 9999999999999999999999999;
		}
		#table_id caption a{
			padding-top: 0px;
			padding-bottom: 0px;
			background: #75b566;
			color: white;
		}

		#exportBtns{
		    float: right;
		    font-size: 16px;
		    background: #2f69a0;
		    padding: 5px 15px 5px 15px;
		    color: white;
		    margin-top: -5px;
		}

		#pendingButton{
			float: right;
			margin-top: -53px;
			padding: 10px;
			background: #801200;
			color: white;
		}
		.link-footer:hover{
			color: #0088cc;
		}


		</style>

						<script type="text/javascript">


$(document).ready(function() {
    $('#table_id').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
							
								function downloadCSV(csv, filename) {
									    var csvFile;
									    var downloadLink;

									    // CSV file
									    csvFile = new Blob([csv], {type: "text/csv"});

									    // Download link
									    downloadLink = document.createElement("a");

									    // File name
									    downloadLink.download = filename;

									    // Create a link to the file
									    downloadLink.href = window.URL.createObjectURL(csvFile);

									    // Hide download link
									    downloadLink.style.display = "none";

									    // Add the link to DOM
									    document.body.appendChild(downloadLink);

									    // Click download link
									    downloadLink.click();
									}

									function exportTableToCSV(filename) {
							    var csv = [];
							    var rows = document.querySelectorAll("#table_id tr");

							    for (var i = 0; i < rows.length; i++) {
							        var row = [], cols = rows[i].querySelectorAll("td, th");

							        for (var j = 0; j < cols.length; j++)
							            row.push(cols[j].innerText);

							        csv.push(row.join(","));
							    }

							    // Download CSV file
							    downloadCSV(csv.join("\n"), filename);
							}
							
									function exportReportTableToCSV(filename) {
							    var csv = [];
							    var rows = document.querySelectorAll("#Report tr");

							    for (var i = 0; i < rows.length; i++) {
							        var row = [], cols = rows[i].querySelectorAll("td, th");

							        for (var j = 0; j < cols.length; j++)
							            row.push(cols[j].innerText);

							        csv.push(row.join(","));
							    }

							    // Download CSV file
							    downloadCSV(csv.join("\n"), filename);
							}

						</script>


		<script type="text/javascript">
			$(document).ready( function () {
			$('#table_id').DataTable();
			} );
		</script>
		<script>
			$('.asdfsa').fdatepicker();
			$('.datepick').fdatepicker({
			  format: 'dd/mm/yyyy',
			  pickTime: false
			});
			$('#demo-3').fdatepicker();
			var nowTemp = new Date();
			var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
			var checkin = $('#dpd1').fdatepicker({
				onRender: function (date) {
					return date.valueOf() < now.valueOf() ? 'disabled' : '';
				}
			}).on('changeDate', function (ev) {
				if (ev.date.valueOf() > checkout.date.valueOf()) {
					var newDate = new Date(ev.date)
					newDate.setDate(newDate.getDate() + 1);
					checkout.update(newDate);
				}
				checkin.hide();
				$('#dpd2')[0].focus();
			}).data('datepicker');
			var checkout = $('#dpd2').fdatepicker({
				onRender: function (date) {
					return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
				}
			}).on('changeDate', function (ev) {
				checkout.hide();
			}).data('datepicker');
		</script>
	</body>
</html>
