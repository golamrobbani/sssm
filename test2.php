<?php
include 'header.php';

//$email   = 'golamrobbani29@gmail.com';
//$sendto  = $email;
$subject = 'Date Expire Product Notification';

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: <smartsupershop@pos.jobsolutionbd.com>' . "\r\n";

$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Date Expire Product List</title>
<style type="text/css">

 #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

body{margin:0; padding:0;}
img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
table{border-collapse:collapse !important;}
body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}


#bodyCell{padding:20px;}
#templateContainer{width:600px;}

body, #bodyTable{
  /*@editable*/ background-color:#DEE0E2;
}


#bodyCell{
/*@editable*/ border-top:4px solid #BBBBBB;
}

 #templateContainer{
/*@editable*/ border:1px solid #BBBBBB;
}


h2{
  /*@editable*/ color:#606060 !important;
  display:block;
  /*@editable*/ font-family:Helvetica;
  /*@editable*/ font-size:16px;

  /*@editable*/ font-weight:normal;
  /*@editable*/ line-height:100%;
  /*@editable*/ letter-spacing:normal;
  margin-top:0;
  margin-right:0;
  margin-bottom:10px;
  margin-left:0;
  /*@editable*/ text-align:left;
}




.preheaderContent{
  /*@editable*/ color:#808080;
  /*@editable*/ font-family:Helvetica;
  /*@editable*/ font-size:10px;
  /*@editable*/ line-height:125%;
  /*@editable*/ text-align:left;
}

/ 
.preheaderContent a:link, .preheaderContent a:visited, /* Yahoo! Mail Override */ .preheaderContent a .yshortcuts /* Yahoo! Mail Override */{
  /*@editable*/ color:#606060;
  /*@editable*/ font-weight:normal;
  /*@editable*/ text-decoration:underline;
}




            #templateBody{
/*@editable*/ background-color:#F4F4F4;
/*@editable*/ border-top:1px solid #FFFFFF;
/*@editable*/ border-bottom:1px solid #CCCCCC;
}

.bodyContent{
  /*@editable*/ color:#505050;
  /*@editable*/ font-family:Helvetica;
  /*@editable*/ font-size:14px;
  /*@editable*/ line-height:150%;
  padding-top:20px;
  padding-right:20px;
  padding-bottom:20px;
  padding-left:20px;
  /*@editable*/ text-align:left;
}

.bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */{
  /*@editable*/ color:#EB4102;
  /*@editable*/ font-weight:normal;
  /*@editable*/ text-decoration:underline;
}

.bodyContent img{
  display:inline;
  height:auto;
  max-width:560px;
}




@media only screen and (max-width: 480px){

  body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
  body{width:100% !important; min-width:100% !important;}  
                #bodyCell{padding:10px !important;}


                #templateContainer{
  max-width:600px !important;
  /*@editable*/ width:100% !important;
}


h2{
  /*@editable*/ font-size:20px !important;
  /*@editable*/ line-height:100% !important;
}




 #templatePreheader{display:none !important;} /* Hide the template preheader to save space */



.headerContent{
  /*@editable*/ font-size:20px !important;
  /*@editable*/ line-height:125% !important;
}


.bodyContent{
  /*@editable*/ font-size:18px !important;
  /*@editable*/ line-height:125% !important;
}
}
</style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
<tr>
<td align="center" valign="top" id="bodyCell">
<!-- BEGIN TEMPLATE // -->
<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">


<tr>
<td align="center" valign="top">
<!-- BEGIN BODY // -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">';

?>



<?php
$expire_product_list=false;
$current_date   = date("Y-m-d");
$expire_product = mysqli_query($conn, "SELECT * FROM  product_details where expire_date='$current_date' order by id DESC");
while ($expire_product_data = mysqli_fetch_array($expire_product)) {

    $expire_product_list=true;
$message.='<tr>

<td valign="top" class="bodyContent" mc:edit="body_content">
'.
    $expire_product_data['name']

.'</td>


</tr>';

}



$message.='</table>
<!-- // END BODY -->
</td>
</tr>
<tr>

</tr>
</table>
<!-- // END TEMPLATE -->
</td>
</tr>
</table>

</body>
</html>';





$admin_user = mysqli_query($conn, "SELECT * FROM  users where usertype='admin' order by id DESC");
while ($admin_user_data = mysqli_fetch_array($admin_user)) {


    if ($expire_product_list && !empty($admin_user_data['email'])) {

      //echo $sendto.'<br/>';
      //echo $subject.'<br/>';
      //echo $message.'<br/>';
      //echo $headers.'<br/>';

        $email   = mail($admin_user_data['email'], $subject, $message, $headers);

        if ($email){
            echo 'email successfuly  send';
        }else{
            echo 'email not  send';
        }



    }

}
?>

<?php include 'footer.php';?>


