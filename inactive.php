<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Inactive Product Details</div>
		<div class="panel">
			<!--View-->
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}
				
			?>
			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>UPC/EAN/ISBN</th>
						<th>Product Name</th>     
						<th><a href="product_add_inactive.php" id="example1" class="view btn btn-primary">Add Inactive Product</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysql_query("SELECT * FROM  product_details where status='inactive' order by id DESC");
				 
					while ($info = mysql_fetch_array($query)) 
					{
					?>
					<tr>
						<td><?php echo $info['product_code'];?></td>
						<td><?php echo $info['name'];?></td>
						<td width="200">
							<span class="pull-right">
								<form action="inactive_action.php?id=<?php echo $info['id'];?>" method="POST" enctype="multipart/form-data">
								<div class="hide">
									<input type="text" id="name" name="name" value="<?php echo $info['name'];?>" required>
								</div>
								<div class="hide">
									<input type="text" id="status" name="status" value="active" required>
								</div>
								<button type="submit" class="view btn-success">Active</button>
								</form>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
