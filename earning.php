<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Earning Details</div>
		<div class="panel">
			<!--View-->
			<?php
				if($_POST)
				{
					$date = $_POST['date'];
					$date = str_replace('/', '-', $date);
					$date = strtotime($date);
					$comments = $_POST['comments'];
					$earning_head = $_POST['earning_head'];
					$amount = $_POST['amount'];
					$req="INSERT INTO earning (date, comments, earning_head, amount)  VALUES ('$date', '$comments', '$earning_head', '$amount')";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='earning.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='earning.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Date</th>
						<th>Earning Head Name</th>
						<th>Earning Amount</th>
						<th>Comments</th>
						<th><a href="earning_add.php" id="example1" class="view btn btn-primary">Add Earning</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysqli_query($conn, "SELECT * FROM  earning order by id DESC");

					while ($info = mysqli_fetch_array($query))
					{
					?>
					<tr>
							<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td><?php echo $info['earning_head'];?></td>
						<td>Tk <?php echo $info['amount'];?></td>
						<td><?php echo $info['comments'];?></td>

						<td width="150">
							<span class="pull-right">
								<a title="View" href="earning_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="earning_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="earning_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
