<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Recive Details</div>
		<div class="panel">
			<!--View-->

			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Date</th>
						<th>Order No.</th>
						<th>Customer Name</th>
						<th>Payable Amount</th>
						<th>Paid Amount</th>
						<th>Due Amount</th>
					  <th>Recive Now</th>

					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysqli_query($conn, "SELECT * FROM  sales where due>0");
					while ($info = mysqli_fetch_array($query))
					{
							$cid = $info['customer_id'];
							$id = $info['id'];
							$due = $info['due'];
						$q = mysqli_query($conn, "SELECT sum(payment) FROM  recive where customer_id='$cid' and order_id='$id'");
						while ($in = mysqli_fetch_array($q))
						{
							$totalPaid = $in['sum(payment)'];
						}

						if($due > $totalPaid){


						?>
					<tr>
						<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td>#<?php echo $info['id'];?></td>
						<td><?php echo $info['customer'];?></td>
						<td>Tk <?php echo number_format($info['payable'],2);?></td>
						<td>Tk <?php echo number_format($info['paid'] + $totalPaid,2);?></td>
						<td>Tk <?php echo number_format($info['due'] - $totalPaid,2);?></td>

						<td>
							<span class="pull-right">
							  <a title="Recive" href="outstanding_now.php?id=<?php echo $info['id'];?>&cid=<?php echo $info['customer_id'];?>" id="example1" class="view btn-primary">Recive Now</a>
								</span>
							</span>
						</td>
					</tr>
				<?php } }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
