<?php
	include 'pos.php';
?>
<!DOCTYPE html>
<html>
	<head>
	<title>:: SSSM ::</title>
	<meta charset="UTF-8">

	<link rel="shortcut icon" href="images/sssm.png" />
	<link rel="apple-touch-icon-precomposed" href="images/sssm.png" />


	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/foundation-datepicker.css" >
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
	<link rel="stylesheet" type="text/css" href="css/droplinebar.css">
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />
	<link rel="stylesheet" href="fancybox/style.css" />

<script
  src="https://code.jquery.com/jquery-1.4.2.min.js"
  integrity="sha256-4joqTi18K0Hrzdj/wGed9xQOt/UuHuur+CeogYJkPFk="
  crossorigin="anonymous"></script>

	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/jquery.dataTables.min.js"></script>

	<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>

<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 -->
<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
 -->
<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	 -->
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>



	





	<script src="js/scripts.js"></script>
	<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
    <script src="js/jquery.load.js"></script>
	<script src="js/foundation-datepicker.js"></script>
	<script src="js/select2.min.js"></script>


	<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>



		<script type="text/javascript">
			$(document).ready(function() {
				/*
				*   Examples - images
				*/

				$("a#example1").fancybox();

				$("a#example2").fancybox({
					'overlayShow'	: false,
					'transitionIn'	: 'elastic',
					'transitionOut'	: 'elastic'
				});

				$("a#example3").fancybox({
					'transitionIn'	: 'none',
					'transitionOut'	: 'none'
				});

				$("a#example4").fancybox({
					'opacity'		: true,
					'overlayShow'	: false,
					'transitionIn'	: 'elastic',
					'transitionOut'	: 'none'
				});

				$("a#example5").fancybox();

				$("a#example6").fancybox({
					'titleposition'		: 'outside',
					'overlayColor'		: '#000',
					'overlayOpacity'	: 0.9
				});

				$("a#example7").fancybox({
					'titleposition'	: 'inside'
				});

				$("a#example8").fancybox({
					'titleposition'	: 'over'
				});

				$("a[rel=example_group]").fancybox({
					'transitionIn'		: 'none',
					'transitionOut'		: 'none',
					'titleposition' 	: 'over',
					'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
						return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
					}
				});

				/*
				*   Examples - various
				*/

				$("#various1").fancybox({
					'titleposition'		: 'inside',
					'transitionIn'		: 'none',
					'transitionOut'		: 'none'
				});

				$("#various2").fancybox();

				$("#various3").fancybox({
					'width'				: '75%',
					'height'			: '75%',
					'autoScale'			: false,
					'transitionIn'		: 'none',
					'transitionOut'		: 'none',
					'type'				: 'iframe'
				});

				$("#various4").fancybox({
					'padding'			: 0,
					'autoScale'			: false,
					'transitionIn'		: 'none',
					'transitionOut'		: 'none'
				});
			});
		</script>

		<script type="text/javascript">

			function PrintElem(elem)
			{
				Popup($(elem).html());
			}
			function Popup(data)
			{
				var mywindow = window.open('', 'my div', 'height=400,width=600');
				mywindow.document.write('<html><head><title>Individual Report</title>');
				/*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
				mywindow.document.write('</head><body >');
				mywindow.document.write(data);
				mywindow.document.write('</body></html>');
				mywindow.document.close(); // necessary for IE >= 10
				mywindow.focus(); // necessary for IE >= 10
				mywindow.print();
				mywindow.close();
				return true;
			}
		</script>

		<script type="text/javascript">
		function startTime() {
			var d = new Date();
			var x = formatAMPM(d);

			var month = new Array();
			month[0] = "January";
			month[1] = "February";
			month[2] = "March";
			month[3] = "April";
			month[4] = "May";
			month[5] = "June";
			month[6] = "July";
			month[7] = "August";
			month[8] = "September";
			month[9] = "October";
			month[10] = "November";
			month[11] = "December";
			var n = month[d.getMonth()];
			var da = d.getDate();

			var day = new Date();
			var weekday = new Array(7);
			weekday[0] = "Sunday";
			weekday[1] = "Monday";
			weekday[2] = "Tuesday";
			weekday[3] = "Wednesday";
			weekday[4] = "Thursday";
			weekday[5] = "Friday";
			weekday[6] = "Saturday";

			var nt = weekday[day.getDay()];


			document.getElementById('watch').innerHTML = n+' '+da+' '+d.getFullYear()+' | '+x +', '+nt;

		}

		function formatAMPM(date) {
		  var hours = date.getHours();
		  var minutes = date.getMinutes();
		  var ampm = hours >= 12 ? 'PM' : 'AM';
		  hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return strTime;
		}


		</script>

	</head>
