<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Product Purchase Return Report</div>
		<div class="panel">

		<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
			 	</table>
					<a href="purchase_return_pending.php" id="pendingButton" >Pending Payment</a>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id " class="display table table-bordered">
					<thead>
						<tr>
							<th>Date</th>
							<th>Invoice Id</th>
							<th>Supplier</th>
							<th>Payable</th>
							<th>Discount</th>
							<th>Paid</th>
							<th>Due</th>
							<th>Payment Method</th>
					<th></th>	</tr>
					</thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{

						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);

					$purchase = mysqli_query($conn, "SELECT * FROM  purchases_return where date between '$from' and '$to' order by id desc");
					$req = mysqli_query($conn, "SELECT sum(payable),sum(due),sum(paid),sum(discount) FROM  purchases_return where date between '$from' and '$to' order by id desc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(payable)'];
				 	 $paid=$data['sum(paid)'];
				 	 $discount=$data['sum(discount)'];
				 	 $due=$data['sum(due)'];
					}

					while ($info = mysqli_fetch_array($purchase))
					{
					$receive_by=$info['reciveby'];
					?>
					<tr>
						<td align="center"><?php echo date("d-m-Y", $info['date']); ?></td>
						<td align="center">BILL-<?php echo $info['id'];?></td>
						<td align="center"><?php echo $info['supplier'];?></td>
				<td align="center">Tk <?php echo number_format($info['payable'], 2);?></td> 
						<td align="center">Tk <?php echo number_format($info['discount'], 2);?></td>
						<td align="center">Tk <?php echo number_format($info['paid'], 2);?></td> 
						<td align="center">Tk <?php echo number_format($info['due'], 2);?></td>  
						<td align="center"><?php  echo $info['pmethod'] == 'cash' ? 'Cash' : 'Chaque/TT';?></td>

						<td>
							<span class="pull-right">
 

									<a title="View"  target="_blank"  href="report_purchase_return_view.php?inv=<?php echo $info['id'];?>"  class="view btn-success">View</a>

								<a title="Delete" href="report_purchase_return_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php	 } }
					else
					{
					$purchase = mysqli_query($conn, "SELECT * FROM  purchases_return order by id desc");
						$req = mysqli_query($conn, "SELECT sum(payable),sum(due),sum(paid),sum(discount) FROM  purchases_return order by id desc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(payable)'];
				 	 $paid=$data['sum(paid)'];
				 	 $discount=$data['sum(discount)'];
				 	 $due=$data['sum(due)'];
					}
					while ($info = mysqli_fetch_array($purchase))
					{
					$receive_by=$info['reciveby'];
					?>
					<tr>
						<td align="center"><?php echo date("d-m-Y", $info['date']); ?></td>
						<td align="center">BILL-<?php echo $info['id'];?></td>
						<td align="center"><?php echo $info['supplier'];?></td>
				<td align="center">Tk <?php echo number_format($info['payable'], 2);?></td> 
						<td align="center">Tk <?php echo number_format($info['discount'], 2);?></td>
						<td align="center">Tk <?php echo number_format($info['paid'], 2);?></td> 
						<td align="center">Tk <?php echo number_format($info['due'], 2);?></td>  
						<td align="center"><?php  echo $info['pmethod'] == 'cash' ? 'Cash' : 'Chaque/TT';?></td>

						<td>
							<span class="pull-right">
								 
									<a title="View"  target="_blank"  href="report_purchase_return_view.php?inv=<?php echo $info['id'];?>"   class="view btn-success">View</a>

								<a title="Delete" href="report_purchase_return_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php } }?>
					</tbody>
				</table>
							<br />
	<br />
	<table>
		<tr>
			<th align="left">Total Purchases : </th>
			<td align="right"><?php echo $payable; ?> Tk </td>
		</tr>
		<tr>
			<th align="left">Total Payment : </th>
			<td align="right"><?php echo $paid; ?> Tk </td>
		</tr>
		<tr>
			<th align="left">Total Discount : </th>
			<td align="right"><?php echo $discount; ?> Tk </td>
		</tr>
		<tr>
			<th align="left">Total Due : </th>
			<td align="right"><?php echo $due; ?> Tk </td>
		</tr>
	</table>


			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
