<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Recive Report</div>
		<div class="panel">
		<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
			 	</table>
				</form>
			 		<div class="panel">
			 			<!--View-->

						<table id="table_id" class="display table table-bordered">
			 				<thead>
			 					<tr>
			 						<th>Order No.</th>
			 						<th>Date</th>
			 						<th>Customer Name</th>
			 						<th>Payable Amount</th>
			 						<th>Paid Amount</th>
			 						<th>Due Amount</th>
			 					  <th>Recive Now </th>
			 					</tr>
			 				</thead>
			 				<tbody>
			 					<?php
			 					$query = mysqli_query($conn, "SELECT * FROM  sales where due>0 order by id asc");
			 					while ($info = mysqli_fetch_array($query))
			 					{
			 							$cid = $info['customer_id'];
			 							$id = $info['id'];
			 							$due = $info['due'];
			 						$q = mysqli_query($conn, "SELECT sum(payment) FROM  recive where customer_id='$cid' and order_id='$id'");
			 						while ($in = mysqli_fetch_array($q))
			 						{
			 							$totalPaid = $in['sum(payment)'];
			 						}

			 						if($due > $totalPaid){


			 						?>
			 					<tr>
			 						<td>#<?php echo $info['id'];?></td>
			 						<td><?php echo date("d-m-Y", $info['date']); ?></td>
			 						<td><?php echo $info['customer'];?></td>
			 						<td>Tk <?php echo number_format($info['payable'],2);?></td>
			 						<td>Tk <?php echo number_format($info['paid'] + $totalPaid,2);?></td>
			 						<td>Tk <?php echo number_format($info['due'] - $totalPaid,2);?></td>

			 						<td>
			 							<span class="pull-right">
			 							  <a title="Recive" href="outstanding_now.php?id=<?php echo $info['id'];?>&cid=<?php echo $info['customer_id'];?>" id="example1" class="view btn-primary">Recive Now</a>
			 								</span>
			 							</span>
			 						</td>
			 					</tr>
			 				<?php } }?>
			 				</tbody>
			 			</table>
			 		</div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
