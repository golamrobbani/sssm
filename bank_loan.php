<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Bank Loan Details</div>
		<div class="panel">
			<!--View-->
			<?php
				if($_POST)
				{
					$date = $_POST['date'];
					$date = str_replace('/', '-', $date);	
					$date = strtotime($date);
					$bank_name = $_POST['bankname'];
					$amount = $_POST['amount'];
					$bank_ac_name_name = $_POST['acname'];
					$branch = $_POST['branchname'];
					$payable_amount = $_POST['payable_amount'];
					$interest = $_POST['interest'];
					$installment = $_POST['installment'];
					$type = 'bank';
					$bank_ac_number = $_POST['acnumber'];
					$comments = $_POST['comments'];
					$req="INSERT INTO loan (name, date, amount,interest,payable_amount,type, bank_name,bank_ac_name,branch_name,total_installment,  bank_ac_name_name,comments)  VALUES ('$bank_name', '$date', '$amount', '$interest', '$payable_amount', '$type', '$bank_name', '$bank_ac_number', '$branch', '$installment', '$bank_ac_name_name', '$comments')";
					if (mysql_query($req))
					{
						echo"<script>location.href='bank_loan.php?message=success'</script>";
					}
					else 
					{
						echo"<script>location.href='bank_loan.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>'; 
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}
				
			?>
			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Date</th>
						<th>Bank Name</th>
						<th>A/C Name</th>
						<th>A/C Number</th>
						<th>Amount</th>
						<th>Payable Amount</th>
						<th>Paid Amount</th>
						<th>Due Amount</th>         
						<th><a href="bank_loan_add.php" id="example1" class="view btn btn-primary">Add Bank Loan</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysql_query("SELECT * FROM  loan where type='bank' order by id DESC");
				 
					while ($info = mysql_fetch_array($query)) 
					{
					$payable_amount=$info['payable_amount'];
					$period=$info['period'];
					$due_amount=$payable_amount-$period;
					?>
					<tr>
						<td><?php echo $info['date'];?></td>
						<td><?php echo $info['bank_name'];?></td>
						<td><?php echo $info['bank_ac_name_name'];?></td>
						<td><?php echo $info['bank_ac_name'];?></td>
						<td>Tk <?php echo $info['amount'];?></td>
						<td>Tk <?php echo $info['payable_amount'];?></td>
						<td>Tk <?php echo $info['period'];?></td>
						<td>Tk <?php echo $due_amount?></td>
						<td width="220">
							<span class="pull-right">
								<a title="Edit" href="bank_loan_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="bank_loan_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
								<a title="Pay History" href="bank_loan_pay_history.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-warning">Pay History</a>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
