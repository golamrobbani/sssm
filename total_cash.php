<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Total Cash Details</div>
		<div class="panel">
			<!--View-->

			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Total Amount</th>
						<th>Bank</th>
						<th>Cash</th>
						<th>Investment</th>
					</tr>
				</thead>
				<tbody>

					<?php
					$query = mysqli_query($conn, "SELECT  SUM(amount)  FROM  earning ");
				 while ($info = mysqli_fetch_array($query))
					{
					 $earning_amount1= $info['SUM(amount)'];
					 }
					$querys = mysqli_query($conn, "SELECT  SUM(amount)  FROM  expense ");
				 while ($infos = mysqli_fetch_array($querys))
					{
					 $earning_amount2= $infos['SUM(amount)'];
					 }
					 $earning_ammount=$earning_amount1-$earning_amount2;

					 $queryss = mysqli_query($conn, "SELECT  SUM(amount)  FROM  investment ");
				 while ($infoss = mysqli_fetch_array($queryss))
					{
					 $investment= $infoss['SUM(amount)'];
					}


						 $purchases = mysqli_query($conn, "SELECT  SUM(paid)  FROM  purchases where status = 1 and pmethod = 'card'");
					 while ($pur = mysqli_fetch_array($purchases))
						{
						 $p_bank= $pur['SUM(paid)'];
						 }

	 					 $pc = mysqli_query($conn, "SELECT  SUM(paid)  FROM  purchases where status = 1 and pmethod != 'card'");
	 				 	while ($puc = mysqli_fetch_array($pc))
	 					{
	 					 $p_cash= $puc['SUM(paid)'];
	 					 }


					 $purchasess = mysqli_query($conn, "SELECT  SUM(paid)  FROM  purchases_return  where status = 1 and pmethod = 'card'");
				 		while ($purs = mysqli_fetch_array($purchasess))
					 {
					 $pr_bank= $purs['SUM(paid)'];
					 }

					 $prc = mysqli_query($conn, "SELECT  SUM(paid)  FROM  purchases_return  where status = 1 and pmethod != 'card'");
						while ($purss = mysqli_fetch_array($prc))
					 {
					 $pr_cash= $purss['SUM(paid)'];
					 }

  					 $bsale = mysqli_query($conn, "SELECT  SUM(paid)  FROM  sales where pmethod = 'card' and status = 1 ");
  				 while ($binfo = mysqli_fetch_array($bsale))
  					{
  					 $bank_sale = $binfo['SUM(paid)'];
  					 }

					 $sales = mysqli_query($conn, "SELECT  SUM(paid)  FROM  sales where status = 1 and pmethod != 'card'");
					 while ($saless = mysqli_fetch_array($sales))
						{
						 $cash_sale = $saless['SUM(paid)'];
						}

					 $saless = mysqli_query($conn, "SELECT  SUM(paid)  FROM  sales_return  where pmethod = 'card' and status = 1 ");
				 while ($salesss = mysqli_fetch_array($saless))
					 {
					 $sr_bank = $salesss['SUM(paid)'];
					 }

 					 $sr = mysqli_query($conn, "SELECT  SUM(paid)  FROM  sales_return  where pmethod != 'card' and status = 1 ");
 				 while ($srs = mysqli_fetch_array($sr))
 					 {
 					 $sr_cash = $srs['SUM(paid)'];
 					 }


					 $bamount = mysqli_query($conn, "SELECT  SUM(amount)  FROM  transfer where type='c2b' ");
						 while ($data = mysqli_fetch_array($bamount))
							{
							 $ban= $data['SUM(amount)'];
							 }

						 $camount = mysqli_query($conn, "SELECT  SUM(amount)  FROM  transfer where type='b2c' ");
						 while ($datas = mysqli_fetch_array($camount))
							{
							 $cash= $datas['SUM(amount)'];
							}


					  	$salary = mysqli_query($conn, "SELECT sum(amount) FROM  salary");
							while ($salary_info = mysqli_fetch_array($salary))
							{
								$esalary=$salary_info['sum(amount)'];
							}

							$mid_cash = $cash + $cash_sale + $earning_ammount + $investment + $pr_cash - $p_cash - $sr_cash - $ban - $esalary;
							$mid_bank =  $bank_sale + $pr_bank - $p_bank - $sr_bank + $ban - $cash;


					 ?>
					<tr>
						<td align="center">Tk  <?php echo $mid_cash + $mid_bank; ?></td>
						<td align="center">Tk  <?php echo $mid_bank; ?></td>
						<td align="center">Tk  <?php echo $mid_cash; ?></td>
						<td align="center">Tk  <?php echo $investment; ?></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
