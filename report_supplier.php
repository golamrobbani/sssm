<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Supplier Report</div>
		<div class="panel">

			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
			 	</table>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id" class="display table table-bordered">
					<thead>
						<tr>
						<th>Date</th>
						<th>Supplier Name</th>
						<th>Total Amount</th>
						<th>Paid Amount</th>
						<th>Due Amount</th>
						<th></th>
						</tr>
					</thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{
						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);

					$purchase = mysqli_query($conn, "SELECT * FROM personinformation where type='supplier'   order by id desc");
					$payable=mysqli_num_rows($purchase);
					while ($info = mysqli_fetch_array($purchase))
					{
					$cid = $info['id'];
					$previous_due = $info['due'];
					$sale = mysqli_query($conn, "SELECT sum(payable) as total, sum(paid) as paid, sum(due) as due FROM purchases where supplier_id='$cid'  and date between '$from' and '$to'  order by id desc");
					while ($summation = mysqli_fetch_array($sale)){
						$total = $summation['total'];
						$paid = $summation['paid'];
						$due = $summation['due'];
					}

					$recives = mysqli_query($conn, "SELECT sum(payment) as payment FROM payment where supplier_id='$cid'  and date between '$from' and '$to'  order by id desc");
					while ($recive = mysqli_fetch_array($recives)){
						$payment = $recive['payment'];
					}

					$due = ($previous_due + $due) - $payment ;
					$paid = $paid + $payment;

					?>
					<tr>
						<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td><a title="View" href="supplier_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-info"><?php echo $info['name'];?></a>
</td>
						<td>Tk <?php echo number_format($total + $previous_due,2);?></td>
						<td>Tk <?php echo number_format($paid,2);?></td>
						<td>Tk <?php echo number_format($due,2);?></td>
						<td>
							<span class="pull-right">
								<?php if($due > 0){ ?>
									<a title="Recive" href="spayment_now.php?id=<?php echo $info['id'];?>&due=<?php echo $due;?>&total=<?php echo $total;?>&paid=<?php echo $paid;?>" id="example1" class="view btn-primary">Payment</a>
								<?php } ?>
								<a title="View" target="_blank" href="supplier_details_report.php?cid=<?php echo $info['id'];?>" class="view btn-success">Details</a>

								<a title="Delete" href="supplier_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php	 } }
					else
					{

					$purchase = mysqli_query($conn, "SELECT * FROM personinformation where type='supplier'  order by id desc");
					$payable=mysqli_num_rows($purchase);
					while ($info = mysqli_fetch_array($purchase))
					{
					$cid = $info['id'];
					$previous_due = $info['due'];
					$sale = mysqli_query($conn, "SELECT sum(payable) as total, sum(paid) as paid, sum(due) as due FROM purchases where supplier_id='$cid' order by id desc");
					while ($summation = mysqli_fetch_array($sale)){
						$total = $summation['total'];
						$paid = $summation['paid'];
						$due = $summation['due'];
					}

					$recives = mysqli_query($conn, "SELECT sum(payment) as payment FROM payment where supplier_id='$cid' order by id desc");
					while ($recive = mysqli_fetch_array($recives)){
						$payment = $recive['payment'];
					}

					$due = ($previous_due + $due) - $payment ;
					$paid = $paid + $payment;

					?>
					<tr>
						<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td><a title="View" href="supplier_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-info"><?php echo $info['name'];?></a>
</td>
						<td>Tk <?php echo number_format($total + $previous_due,2);?></td>
						<td>Tk <?php echo number_format($paid,2);?></td>
						<td>Tk <?php echo number_format($due,2);?></td>
						<td>
							<span class="pull-right">
								<?php if($due > 0){ ?>
									<a title="Recive" href="spayment_now.php?id=<?php echo $info['id'];?>&due=<?php echo $due;?>&total=<?php echo $total;?>&paid=<?php echo $paid;?>" id="example1" class="view btn-primary">Payment</a>
								<?php } ?>
								<a title="View" target="_blank" href="supplier_details_report.php?cid=<?php echo $info['id'];?>" class="view btn-success">Details</a>

								<a title="Delete" href="supplier_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php } } ?>
					</tbody>
				</table>
								<br />
	<br />
	<table>
		<tr>
			<th align="left">Total Customer : </th>
			<td align="right"><?php echo $payable; ?> </td>
		</tr>

	</table>
			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
