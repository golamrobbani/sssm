<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Bank Report</div>
		<div class="panel">
				<?php
				if($_POST)
				{
					$date = date('d-m-Y');
					$date = strtotime($date);
					$name = $_POST['name'];
					$ptype = $_POST['ptype'];
					$bname = $_POST['bname'];
					$aname = $_POST['aname'];
					$anumber = $_POST['anumber'];
					$comments = $_POST['comments'];
					$req="INSERT INTO bankinformation (date,person, person_type, bankname, accountname,accountnumber,comments)  VALUES ('$date','$name', '$ptype', '$bname', '$aname', '$anumber', '$comments')";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='bank_report.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='bank_report.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
						<a value="Print Report" href="#" onclick="PrintElem('#mydiv')"  class="btn-add btn-warning pull-right">Print Report</a>
				</table>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id" class="display table table-bordered">
					<thead>
						<tr>
						<th>Person Name</th>
						<th>Person Type</th>
						<th>Bank Name</th>
						<th>Account Name</th>
						<th>Account Number</th>
						<th>Comments</th>
					<th><a href="bank_add.php" id="example1" class="view btn btn-primary">Add Bank Information</a></th>	</tr>
					</thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{
						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);

					$purchase = mysqli_query($conn, "SELECT * FROM  bankinformation where date between '$from' and '$to' order by id desc");
					while ($info = mysqli_fetch_array($purchase))
						{

					?>

					<tr>
							<td><?php echo $info['person'];?></td>
						<td><?php echo $info['person_type'];?></td>
						<td><?php echo $info['bankname'];?></td>
						<td><?php echo $info['accountname'];?></td>
						<td><?php echo $info['accountnumber'];?></td>
						<td><?php echo $info['comments'];?></td>
						<td>
							<span class="pull-right">
									<a title="View" href="bank_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">View</a>
									<a title="Edit" href="bank_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
										<a title="Delete" href="bank_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a></span>
							</span>
						</td>
					</tr>
					<?php		  } }
					else
					{
						$purchase = mysqli_query($conn, "SELECT * FROM  bankinformation order by id desc");
					while ($info = mysqli_fetch_array($purchase))
					{

					?>

					<tr>
						<td><?php echo $info['person'];?></td>
						<td><?php echo $info['person_type'];?></td>
						<td><?php echo $info['bankname'];?></td>
						<td><?php echo $info['accountname'];?></td>
						<td><?php echo $info['accountnumber'];?></td>
						<td><?php echo $info['comments'];?></td>
						<td>
							<span class="pull-right">
									<a title="View" href="bank_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">View</a>
									<a title="Edit" href="bank_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
										<a title="Delete" href="bank_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a></span>
							</span>
						</td>
					</tr>
					<?php    } } ?>
					</tbody>
				</table>
			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
