<?php
include 'pos.php';
?>
<form action="" method="POST" class="form">
    <table class="tab">
    	<tr>
    	<td align="right">Date</td>
    	<td><input type="text" name="date" id="date" value="<?php echo date('d/m/Y');?>" required></td>
    	</tr>
		<tr>
    	<td align="right">Expense Head Name</td>
    	<td>
		<select name="expense_head" id="expense_head"required>
			<option value="">--- Select ---</option>
			<?php
				$data=mysqli_query($conn, "SELECT * FROM expense_head");
				while($data_info=mysqli_fetch_array($data))
				{
				$head_name= $data_info['head'];
				?>
				<option value="<?php echo $head_name;?>"><?php echo $head_name;?></option>
			<?php }?>
		</select>
		</td>
    	</tr>
		<tr>
    	<td align="right">Expense Amount</td>
    	<td><input  type="number" required=""  step="0.01"  name="amount" id="amount"required></td>
    	</tr>
    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right">Comments</td>
    	<td><textarea placeholder="Enter Comments" name="comments" id="comments" rows="5" cols="20"></textarea></td>
    	</tr>
    	<tr>
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input type="submit" class="view btn-success" value="Submit"></td>
    	</tr>
    </table>
</form>
