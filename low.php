<?php include 'header.php';?>

	<div class="area">
		<div class="panel-head">Low Stock</div>
		<div class="panel">
			<!--View-->

			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Product Code</th>
						<th>Product Name</th>
						<th>Category Name</th>
						<th>Retail Price</th>
						<th>Expiery Date</th>
						<th>Stock Amount</th>
						<th><a href="product_add.php" id="example1" class="view btn btn-primary">Add Product</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysqli_query($conn, "SELECT * FROM  product_details where status='active' and stock_id<=10 order by id DESC");
					while ($info = mysqli_fetch_array($query))
					{?>
					<tr>
						<td><?php echo $info['product_code'];?> </td>
						<td><?php echo $info['name'];?></td>
						<td><?php echo $info['category'];?></td>
						<td>Tk  <?php echo $info['sale_price'];?></td>
						<td><?php echo $info['expire_date'];?></td>
						<td><?php echo $info['stock_id'];?></td>
						<td width="140">
							<span class="pull-right">
								<a title="View" href="product_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="product_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="product_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
