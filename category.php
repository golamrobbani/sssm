<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Category Details</div>
		<div class="panel">
			<!--View-->
			<?php
				if($_POST)
				{
					$category_name = $_POST['category_name'];
					$comments = $_POST['comments'];
					$description = $_POST['description'];
					$req="INSERT INTO category (category_name, comments, description)  VALUES ('$category_name', '$comments', '$description')";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='category.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='category.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Category Name</th>
						<th>Category Description</th>
						<th>Category Comments</th>
						<th><a href="category_add.php" id="example1" class="view btn btn-primary">Add Category</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysqli_query($conn, "SELECT * FROM  category order by id DESC");
					while ($info = mysqli_fetch_array($query))
					{
					?>
					<tr>
						<td><?php echo $info['category_name'];?></td>
						<td><?php echo $info['description'];?></td>
						<td><?php echo $info['comments'];?></td>
						<td width="120">
							<span class="pull-right">
								<a title="Edit" href="category_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="category_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
