<form action="" method="POST" class="form">
    <table class="tab">
    	<tr>
			<td align="right">Employee Name</td>
			<td><input type="text" name="name" id="name" placeholder="Enter Employee name"required></td>
    	</tr>
		<tr>
			<td align="right">Employee Designation</td>
			<td><input type="text" name="designation" id="designation" placeholder="Enter Employee designation"required></td>
    	</tr>
		<tr>
			<td align="right">Employee Salary</td>
			<td><input type="text" name="salary" id="salary" placeholder="Enter Employee salary"required></td>
    	</tr>
    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right">Employee Address</td>
    	<td><textarea placeholder="Enter Employee Address" name="address" id="address" rows="5" cols="20"></textarea></td>
    	</tr>
    	<tr>
    	<td align="right">Employee Phone</td>
    	<td><input type="text" name="phone" id="phone" placeholder="Enter Employee phone"></td>
    	</tr>

    	<tr>
    	<td align="right">Employee Email</td>
    	<td><input type="text" name="email" id="email" placeholder="Enter Employee email"></td>
    	</tr>

    	<tr class="hide">
    	<td valign="top" style="padding-top:20px;" align="right">Employee Type</td>
    	<td><input value="employee" name="type" id="type"></td>
    	</tr>
    	<tr>
    	
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input onclick="return confirm('Are you sure?')" class="view btn-success" type="submit" value="Submit"></td>
    	</tr>
    </table>
</form>