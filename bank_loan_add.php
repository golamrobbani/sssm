<script type="text/javascript">
function calculate() {
    var myq3 = document.getElementById('q1').value; 
    var myu3 = document.getElementById('q2').value;
    var result = document.getElementById('q3'); 
	if(myu3==''){
		result.value = myq3;
	}
	if(myq3!='' && myu3!=''){
		x = parseInt(myu3)*parseInt(myq3);
		y = parseInt(x)/100;
		r = parseInt(y)+parseInt(myq3);
		result.value = r.toFixed(2);

	}
}
function view_loan(val){
	$('#view_loan tr').remove(); 
	$.POST('view_loan.php', {productid: val},
	function (data) {
		$(data).fadeIn("slow").appendTo('#view_loan');
		
		
	});
	
	
}
function payment_history(val){
	$('.view_loan').closest("tr").remove();
	$.POST('payment_history.php', {productid: val},
	function (data) {
		$(data).fadeIn("slow").appendTo('#view_loan');
		
		
	});
}
</script>
	<script type="text/javascript">
function setSelectedValue(selectObj, valueToSet) {
    for (var i = 0; i < selectObj.options.length; i++) {
        if (selectObj.options[i].text== valueToSet) {
            selectObj.options[i].selected = true;
            return;
        }
    }
}
function checkproductdetails(val){
	
	$.POST('check_loan_personal.php', {productid: val},
	function (data) {
		
		$("#date").val(data.date);
		$("#bankname").val(data.bankname);
		$("#acname").val(data.acname);
		$("#acnumber").val(data.acnumber);
		$("#branchname").val(data.branchname);
		$("#q1").val(data.amount);
		$("#q2").val(data.interest);
		$("#q3").val(data.payable_amount);
		$("#installment").val(data.installment);
	
		$("#comments").val(data.comments);
	}, 'json');
	
	$('#hidden_field').html('<input type="hidden" name="id" value='+val+' />');
}
function emptyform(){
		
		$("#bankname").val('');
		$("#acname").val('');
		$("#acnumber").val('');
		$("#branchname").val('');
		$("#q1").val('');
		$("#q2").val('');
		$("#q3").val('');
		$("#installment").val('');
		$("#comments").val('');
		$("#hidden_field").html('');

}
</script>
<div class="modalform">
    	Add a Bank Loan<hr/>
    	<form action="" method="POST" class="form">
    	<table class="tab">
		<tr>
    	<td align="right">Date</td>
    	<td><input type="text" name="date" id="date" class="datepick" value="30-01-2018" /></td>
    	</tr>
        <tr>
        <td align="right">Bank Name</td>
        <td><input type="text" name="bankname" id="bankname" ></td>
        </tr>
        <tr>
        <td align="right">A/C Name </td>
        <td><input type="text" name="acname" id="acname" ></td>
        </tr>
         <tr>
        <td align="right">A/C Number </td>
        <td><input type="text" name="acnumber" id="acnumber" ></td>
        </tr>
        <tr>
        <td align="right">Branch Name </td>
        <td><input type="text" name="branchname" id="branchname" ></td>
        </tr>
        <tr>
        <tr>
        <td align="right">Amount </td>
        <td><input type="text" id="q1" name="amount" oninput="calculate()" ></td>
        </tr>
         <tr>
        <td align="right">Interest </td>
        <td><input type="text" id="q2" name="interest" oninput="calculate()"></td>
        </tr>
         <tr>
        <td align="right">Payable Amount </td>
        <td><input type="text" id="q3" name="payable_amount" ></td>
        </tr>
        <tr>
        <td align="right">Toatl Installment</td>
        <td><input type="text" name="installment" id="installment"></td>
        </tr>
    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right"> Comments</td>
    	<td><textarea placeholder="Enter Comments" name="comments" id="comments" rows="5" cols="20"></textarea></td>
    	</tr>
    	<tr>
    	
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input type="submit" value="Go"></td>
    	</tr>
    	</table>
    	</form>
</div>