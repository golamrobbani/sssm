<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Reports</div>
		<div class="panel">
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
						<a value="Print Report" href="#" onclick="PrintElem('#myd3iv')"  class="btn-add btn-warning pull-right">Print Report</a>
				</table>
				</form>
				<div style="overflow:hidden;padding:10px;margin-bottom:10px;" >


		<?php
					if(isset($_GET['Submit']))
					{


						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);


					$salary = mysqli_query($conn, "SELECT sum(amount) FROM  salary where date between '$from' and '$to'");
						while ($salary_info = mysqli_fetch_array($salary))
						{
							$esalary=$salary_info['sum(amount)'];
						}

							$earning = mysqli_query($conn, "SELECT sum(amount) FROM  earning where date between '$from' and '$to'");
						while ($earning_info = mysqli_fetch_array($earning))
						{
							$eamount=$earning_info['sum(amount)'];
						}

						$expence = mysqli_query($conn, "SELECT SUM(amount) FROM  expense where date between '$from' and '$to'");
						while ($expence_info = mysqli_fetch_array($expence))
						{
							$eexpence=$expence_info['SUM(amount)'];
						}

						$payment = mysqli_query($conn, "SELECT SUM(payment) FROM  payment where date between '$from' and '$to'");
						while ($payment_info = mysqli_fetch_array($payment))
						{
							$epayment=$payment_info['SUM(payment)'];
						}

						$recive = mysqli_query($conn, "SELECT SUM(payment) FROM  recive where date between '$from' and '$to'");
						while ($recive_info = mysqli_fetch_array($recive))
						{
							$erecive=$recive_info['SUM(payment)'];
						}
						$purchases = mysqli_query($conn, "SELECT SUM(payable),SUM(paid),SUM(due),SUM(discount)  FROM  purchases where status = 1 and  date between '$from' and '$to'");
						while ($purchases_info = mysqli_fetch_array($purchases))
						{
							$payable=$purchases_info['SUM(payable)'];
							$paid=$purchases_info['SUM(paid)'];
							$due=$purchases_info['SUM(due)'];
							$discount=$purchases_info['SUM(discount)'];
						}

						$purchases_return = mysqli_query($conn, "SELECT  SUM(payable),SUM(paid),SUM(due),SUM(discount)   FROM  purchases_return where status = 1 and  date between '$from' and '$to'");
						while ($purchases_return_info = mysqli_fetch_array($purchases_return))
						{
							$ppayable=$purchases_return_info['SUM(payable)'];
							$ppaid=$purchases_return_info['SUM(paid)'];
							$pdue=$purchases_return_info['SUM(due)'];
							$pdiscount=$purchases_return_info['SUM(discount)'];
						}

						$sale = mysqli_query($conn, "SELECT  SUM(cost),SUM(payable),SUM(paid),SUM(due),SUM(discount) FROM  sales where status = 1 and date between '$from' and '$to'");
						while ($sale_info = mysqli_fetch_array($sale))
						{
							$spayable=$sale_info['SUM(payable)'];
							$spaid=$sale_info['SUM(paid)'];
							$sdue=$sale_info['SUM(due)'];
						 	$sdiscount=$sale_info['SUM(discount)'];
							$cost=$sale_info['SUM(cost)'];
							$sprofit=$spaid-$cost;
						}



						$sales_return = mysqli_query($conn, "SELECT   SUM(cost),SUM(payable),SUM(paid),SUM(due),SUM(discount)  FROM  sales_return where status = 1 and date between '$from' and '$to'");
						while ($sales_return_info = mysqli_fetch_array($sales_return))
						{
							$sspayable=$sales_return_info['SUM(payable)'];
							$sspaid=$sales_return_info['SUM(paid)'];
							$ssdue=$sales_return_info['SUM(due)'];
							$ssdiscount=$sales_return_info['SUM(discount)'];
							$cost=$sales_return_info['SUM(cost)'];
							$pprofit=$cost-$sspaid;
						}


						$invest = mysqli_query($conn, "SELECT  SUM(amount) FROM  investment");
						while ($i = mysqli_fetch_array($invest))
						{
							$iamount=$i['SUM(amount)'];

						}

						$ptotal=0;
						$rtotal=0;
						$p = mysqli_query($conn, "SELECT * FROM  product_details");

							while ($pp = mysqli_fetch_array($p))
						{
							$qantity=$pp['stock_id'];
							$purchase_cost=$pp['purchase_cost'];
							$sale_price=$pp['sale_price'];

							 $pcost=$qantity*$purchase_cost;
							 $ptotal+=$pcost;
							 $rcost=$qantity*$sale_price;
						  	$rtotal+=$rcost;

						}


					}
					else
					{

				  	$salary = mysqli_query($conn, "SELECT sum(amount) FROM  salary");
						while ($salary_info = mysqli_fetch_array($salary))
						{
							$esalary=$salary_info['sum(amount)'];
						}

							$earning = mysqli_query($conn, "SELECT sum(amount) FROM  earning");
						while ($earning_info = mysqli_fetch_array($earning))
						{
							$eamount=$earning_info['sum(amount)'];
						}

						$expence = mysqli_query($conn, "SELECT SUM(amount) FROM  expense");
						while ($expence_info = mysqli_fetch_array($expence))
						{
							$eexpence=$expence_info['SUM(amount)'];
						}

						$payment = mysqli_query($conn, "SELECT SUM(payment) FROM  payment");
						while ($payment_info = mysqli_fetch_array($payment))
						{
							  $epayment=$payment_info['SUM(payment)'];
						}

						$recive = mysqli_query($conn, "SELECT SUM(payment) FROM  recive");
						while ($recive_info = mysqli_fetch_array($recive))
						{
							$erecive=$recive_info['SUM(payment)'];
						}
						$purchases = mysqli_query($conn, "SELECT SUM(payable),SUM(paid),SUM(due),SUM(discount)  FROM  purchases where status = 1");
						while ($purchases_info = mysqli_fetch_array($purchases))
						{
							$payable=$purchases_info['SUM(payable)'];
							$paid=$purchases_info['SUM(paid)'];
							$due=$purchases_info['SUM(due)'];
							$discount=$purchases_info['SUM(discount)'];
						}

						$purchases_return = mysqli_query($conn, "SELECT  SUM(payable),SUM(paid),SUM(due),SUM(discount)  FROM  purchases_return where status = 1");
						while ($purchases_return_info = mysqli_fetch_array($purchases_return))
						{
							$ppayable=$purchases_return_info['SUM(payable)'];
							$ppaid=$purchases_return_info['SUM(paid)'];
							$pdue=$purchases_return_info['SUM(due)'];
							$pdiscount=$purchases_return_info['SUM(discount)'];
						}
						$sale = mysqli_query($conn, "SELECT  SUM(cost),SUM(payable),SUM(paid),SUM(due),SUM(discount) FROM  sales where status = 1");
						while ($sale_info = mysqli_fetch_array($sale))
						{
							$cost=$sale_info['SUM(cost)'];
							$spayable=$sale_info['SUM(payable)'];
							$spaid=$sale_info['SUM(paid)'];
							$sdue=$sale_info['SUM(due)'];
						 	$sdiscount=$sale_info['SUM(discount)'];
              $sprofit=$spaid-$cost;

						}


						$sales_return = mysqli_query($conn, "SELECT  SUM(cost),SUM(payable),SUM(paid),SUM(due),SUM(discount)  FROM  sales_return  where status = 1");
						while ($sales_return_info = mysqli_fetch_array($sales_return))
						{
							$cost=$sales_return_info['SUM(cost)'];
							$sspayable=$sales_return_info['SUM(payable)'];
							$sspaid=$sales_return_info['SUM(paid)'];
							$ssdue=$sales_return_info['SUM(due)'];
							$ssdiscount=$sales_return_info['SUM(discount)'];
						 	$pprofit=$sspaid-$cost;
						}



						$invest = mysqli_query($conn, "SELECT  SUM(amount) FROM  investment");
						while ($i = mysqli_fetch_array($invest))
						{
							$iamount=$i['SUM(amount)'];



						}

						$p = mysqli_query($conn, "SELECT * FROM  product_details");
						$ptotal=0;
						$rtotal=0;
							while ($pp = mysqli_fetch_array($p))
						{
							$qantity=$pp['stock_id'];
							$purchase_cost=$pp['purchase_cost'];
							$sale_price=$pp['sale_price'];

							 $pcost=$qantity*$purchase_cost;
							 $ptotal+=$pcost;
							 $rcost=$qantity*$sale_price;
						  	$rtotal+=$rcost;

						}



					}

					?>

</div>
   <div class="table_data" id="myd3iv">
      <h2>Cash In Hand</h2>



							<table border=1 width="940px" class="tab">
		<thead>
			<tr>
				<th>Total Cash In Hand</th>

			</tr>
		</thead>


		<tbody>
		<tr>
 <td align="center">
 	<?php

  $er=$eamount+$erecive+$ppaid+$spaid;
 //echo '<br/>';
  $ex=$esalary+$eexpence+$paid+$sspaid;

    echo  ($iamount+$er)-$ex;

    ?>Tk </td>

		</tr>
		</tbody>

	</table>
	     <h2>Profit Lose</h2>
		<table border=1 width="940px" class="tab">
		<thead>
			<tr>
				<th>Total Earn</th>
				<th>Total Expense</th>
				<th>Profit/Lose</th>
			</tr>
		</thead>


		<tbody>
		<tr>
			<td align="center"><?php

			 $netp=($eamount+$sprofit)-$pprofit;

			 echo  $netp;

			 ?>Tk </td>
			<td align="center"><?php echo $netl=$esalary+$eexpence; ?>Tk </td>
			<td align="center"><?php echo $netp-$netl; ?> Tk </td>
		</tr>
		</tbody>

	</table>


   <h2>Sale</h2>
		<table border=1 width="940px" class="tab">
		<thead>
			<tr>
				<th>Total Receiveble</th>
				<th>Total Received</th>
				<th>Total Due</th>


			</tr>
		</thead>
		<tbody>
		<tr>
			<td align="center"><?php echo  number_format($spayable,2); ?> Tk </td>
			<td align="center"><?php echo  number_format($spaid,2); ?>Tk </td>
			<td align="center"><?php echo  number_format($sdue,2); ?> Tk </td>

		</tr>
		</tbody>
			</table>
			   <h2>Purshases</h2>
		<table border=1 width="940px" class="tab">
		<thead>
			<tr>
				<th>Total Payable</th>
				<th>Total Paid</th>
				<th>Total Due</th>


			</tr>
		</thead>
				<tbody>
		<tr>
			<td align="center"><?php echo  number_format($payable,2); ?>Tk </td>
			<td align="center"><?php echo number_format($paid,2); ?>Tk </td>
			<td align="center"><?php echo  number_format($due,2); ?> Tk </td>

		</tr>
		</tbody>
	</table>
		 <h2>Purshases Return</h2>
<table border=1 width="940px" class="tab">
<thead>
	<tr>
		<th>Total Receivble</th>
		<th>Total Recive</th>
		<th>Total Due</th>


	</tr>
</thead>
		<tbody>
<tr>
	<td align="center"><?php echo  number_format($ppayable,2); ?>Tk </td>
	<td align="center"><?php echo number_format($ppaid,2); ?>Tk </td>
	<td align="center"><?php echo  number_format($pdue,2); ?> Tk </td>

</tr>
</tbody>
</table>
		<h2>Other Earnings</h2>
		<table border=1 width="940px" class="display">
		<thead>
			<tr>

				<th>Receive Amount</th>
			</tr>
		</thead>
		<tbody>
		<tr>

<td align="center"><?php echo  number_format($eamount,2); ?>Tk </td>
		 </tr>
		</tbody>


	</table>

			<h2> Other Expense</h2>
		<table border=1 width="940px" class="display">
		<thead>
			<tr>

				<th>Payment Amount</th>
			</tr>
		</thead>
		<tbody>
		<tr>

	<td align="center"><?php echo $eexpence; ?>Tk </td>


		</tr>
		</tbody>

	</table>
	<h2>Salary</h2>
		<table border=1 width="940px" class="display">
		<thead>
			<tr>

				<th>Payment Amount</th>
			</tr>
		</thead>
		<tbody>
		<tr>

			<td align="center"><?php echo $esalary; ?>Tk </td>


		</tr>
		</tbody>

	</table>

			   <h2>Investment</h2>
		<table border=1 width="940px" class="display">
		<thead>
			<tr>

				<th>Total Amount</th>
			</tr>
		</thead>
		<tbody>
		<tr>

			<td align="center"><?php echo $iamount; ?>Tk </td>


		</tr>
		</tbody>

	</table>

	<h2> Stock Value</h2>
		<table border=1 width="940px" class="display">
		<thead>
			<tr>

				<th>Total Purchases Amount</th>	<th>Total Retail Amount</th>
			</tr>
		</thead>
		<tbody>

		<tr>

	<td align="center"><?php echo $ptotal; ?> Tk </td>	<td align="center"><?php echo $rtotal; ?> Tk </td>


		</tr>

		</tbody>

	</table>

   </div>
   </div>

</div>
</div>


<?php include 'footer.php';?>
