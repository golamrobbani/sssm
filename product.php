<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Product List 		<a href="#" id="exportBtns" onclick="exportTableToCSV('Products.csv')">Export</a></div>
		<div class="panel">
			<!--View-->
			<?php
				if($_POST)
				{
					$name = $_POST['name'];
					$product_code = $_POST['product_code'];
					$category = $_POST['category'];
					$purchase_cost = $_POST['purchase_cost'];
					$sale_price = $_POST['sale_price'];
					$wholesale_price = $_POST['wholesale_price'];
					$unit_type = $_POST['unit_type'];
					$brand = $_POST['brand'];
					$description = $_POST['description'];
					$vat = $_POST['vat'];
					$warranty = $_POST['warranty'];
					$expire_date = $_POST['expire_date'];
					$product_add_date = $_POST['product_add_date'];
					$comments = $_POST['comments'];
					$status = $_POST['status'];
					$minquantity = $_POST['minquantity'];
					$stock_id = $_POST['stock_id'];

					$req="INSERT INTO product_details (name, product_code, category, purchase_cost, sale_price, wholesale_price, unit_type, brand, description, vat, warranty, expire_date, product_add_date, comments, status, minquantity, stock_id)  VALUES ('$name', '$product_code', '$category', '$purchase_cost', '$sale_price', '$wholesale_price', '$unit_type', '$brand', '$description', '$vat', '$warranty', '$expire_date', '$product_add_date', '$comments', '$status', '$minquantity', '$stock_id')";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='product.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='product.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}
			?>

				<form action="" method="get">
				   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

							<tr>
								<td width="20%">Lot No.</td>

								<td width="20%">
									<select name="lot">
										<option value="">Select Lot No</option>
										<?php

										$q = mysqli_query($conn, "SELECT DISTINCT lot_no FROM  product_details order by lot_no ASC");
										while ($data = mysqli_fetch_array($q)){

										?>
										<option value="<?php echo $data['lot_no']; ?>"><?php echo $data['lot_no']; ?></option>
									<?php } ?>
									</select>
								</td>

								<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show"> </td>

							</tr>
				 	</table>
				</form>

			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Product Code</th>
						<th>Product Name</th>
						<th>Purchase Cost</th>
						<th>Retail Price</th>
						<th>Lot No.</th>
						<th>Stock</th>
						<th>Sold</th>
						<th>Status</th>
						<th>

							<a href="product_add.php" id="example1" class="view btn btn-primary">Add Product</a></th>
					</tr>
				</thead>
				<tbody>
					<?php

					if(isset($_GET['Submit']))
					{

					$lot = $_GET['lot'];

					$query = mysqli_query($conn, "SELECT * FROM  product_details where lot_no='$lot' order by id DESC");
					while ($info = mysqli_fetch_array($query)){
					$psid = $info['id'];
					$q1 = mysqli_query($conn, "SELECT SUM(stock) as d FROM  demage where product_id = '$psid'");
					$qr = mysqli_fetch_array($q1);
					$demage = $qr['d'];

					$q2 = mysqli_query($conn, "SELECT SUM(quty) as sell FROM  sales_product where item_id = '$psid'");
					$qrr = mysqli_fetch_array($q2);
					$sell = $qrr['sell'];


					 ?>
					<tr>
						<td><?php echo $info['product_code'];?></td>
						<td><?php echo $info['name'];?></td>
						<td>Tk  <?php echo $info['purchase_cost'];?></td>
						<td>Tk  <?php echo $info['sale_price'];?></td>
						<td><?php echo $info['lot_no'];?></td>
						<td><?php echo $info['stock_id'] - $demage ;?></td>
						<td><?php echo $sell;?></td>
						<td><?php echo $info['status'];?></td>
						<td width="140">
							<span class="pull-right">
								<a title="View" href="product_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="product_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="product_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>

					<?php

				}
					}else{


					$query = mysqli_query($conn, "SELECT * FROM  product_details order by id DESC");
					while ($info = mysqli_fetch_array($query)){
					$psid = $info['id'];

					$q1 = mysqli_query($conn, "SELECT SUM(stock) as d FROM  demage where product_id = '$psid'");
					$qr = mysqli_fetch_array($q1);
					$demage = $qr['d'];

					$q2 = mysqli_query($conn, "SELECT SUM(quty) as sell FROM  sales_product where item_id = '$psid'");
					$qrr = mysqli_fetch_array($q2);
					$sell = $qrr['sell'];


					 ?>
					<tr>
						<td><?php echo $info['product_code'];?></td>
						<td><?php echo $info['name'];?></td>
						<td>Tk  <?php echo $info['purchase_cost'];?></td>
						<td>Tk  <?php echo $info['sale_price'];?></td>
						<td><?php echo $info['lot_no'];?></td>
						<td><?php echo $info['stock_id'] - $demage ;?></td>
						<td><?php echo $sell;?></td>
						<td><?php echo $info['status'];?></td>
						<td width="140">
							<span class="pull-right">
								<a title="View" href="product_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="product_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="product_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
