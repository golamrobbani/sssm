<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">User Login Report</div>
		<div class="panel">

			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
						<a value="Print Report" href="#" onclick="PrintElem('#mydiv')"  class="btn-add btn-warning pull-right">Print Report</a>
				</table>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id" class="display table table-bordered">
					<thead>
						<tr>
						<th>Date</th>
						<th>Login Time</th>
						<th>Logout Time</th>
					    <th>Action</th>

					</tr>
					</thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{
						$from=$_GET['from'];
						$to=$_GET['to'];
					$purchase = mysqli_query($conn, "SELECT * FROM  login_details where date between '$from' and '$to' order by id desc");
					while ($info = mysqli_fetch_array($purchase))
						{

					?>

					<tr>
						<td><?php echo $info['date'];?></td>
						<td><?php echo $info['login_time'];?></td>
						<td> <?php echo $info['logout_time'];?></td>


						<td width="150">
							<span class="pull-right">

								<a title="Delete" href="log_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php		  } }
					else
					{
						$purchase = mysqli_query($conn, "SELECT * FROM  login_details order by id desc");
					while ($info = mysqli_fetch_array($purchase))
					{

					?>

					<tr>
						<td><?php echo $info['date'];?></td>
						<td><?php echo $info['login_time'];?></td>
						<td> <?php echo $info['logout_time'];?></td>

						<td width="150">
							<span class="pull-right">

								<a title="Delete" href="log_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php    } } ?>
					</tbody>
				</table>
			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
