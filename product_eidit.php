<?php
include 'pos.php';
?>
<?php
$query = mysqli_query($conn, "SELECT * FROM product_details where id='$_GET[id]'");
while ($info = mysqli_fetch_array($query))
{?>
<form action="product_edit_action.php?id=<?php echo $info['id'];?>" method="POST" class="form">
    <table class="tab">
    	<tr>
    	<td align="right">Product Name</td>
    	<td><input type="text" id="name" name="name" value="<?php echo $info['name'];?>" required></td>
    	</tr>
        <tr>
        <td align="right">Product Code</td>
        <td><input type="text" id="product_code" name="product_code" value="<?php echo $info['product_code'];?>"></td>
        </tr>


        <tr>
            <td align="right">Product Color</td>
            <td><input type="text"  value="<?php echo $info['product_color'];?>" id="product_color" name="product_color" placeholder="Enter Product Color" required></td>
        </tr>


        <tr>
        <td align="right">Product Size</td>
        <td><input type="text" value="<?php echo $info['product_size'];?>" id="product_size" name="product_size" placeholder="Enter Product Size" required></td>
        </tr>



        <tr>
        <td align="right">Product Category</td>
        <td>
            <select class="form-control form" id="category" name="category" required>
				<option value="<?php echo $info['category'];?>"><?php echo $info['category'];?></option>
				<?php
					$sql=mysqli_query($conn, "SELECT * FROM category");
					while($category_info=mysqli_fetch_array($sql))
					{?>
					<option value="<?php echo $category_info['category_name'];?>"><?php echo $category_info['category_name'];?></option>
				<?php }?>
			</select>
        </td>
        </tr>

        <tr>
            <td align="right">Lot Number</td>
            <td><input class="form-control form" id="lot_no" name="lot_no" placeholder="Enter Lot Number" type="number"  value="<?php echo $info['lot_no'];?>"  required=""  step="0.01" ></td>
        </tr>

         <tr>
        <td align="right">Purchase Cost</td>
        <td><input class="form-control form" id="purchase_cost" name="purchase_cost" value="<?php echo $info['purchase_cost'];?>"  type="number" required=""  step="0.01" ></td>
        </tr>
        <tr>
        <td align="right">Retail Sell Price</td>
        <td><input class="form-control form" id="sale_price" name="sale_price" value="<?php echo $info['sale_price'];?>"  type="number" required=""  step="0.01" ></td>
        </tr>

        <tr>
        <td align="right">Whole Sell Price</td>
        <td><input class="form-control form" id="wholesale_price" name="wholesale_price"  value="<?php echo $info['wholesale_price'];?>"  placeholder="Enter whole Sale Price"  type="number" required=""  step="0.01" ></td>
        </tr>

        <tr>
        <td align="right">Stock</td>
        <td><input class="form-control form" id="stock_id" name="stock_id" value="<?php echo $info['stock_id'];?>" type="number"></td>
        </tr>

        <tr>
        <td align="right">Unit Type</td>
        <td><input class="form-control form" id="stock_id" name="unit_type" placeholder="Enter Unit"  value="<?php echo $info['unit_type'];?>" type="text" required=""></td>
        </tr>

        <tr>
        <td align="right">Per Unit</td>
        <td><input class="form-control form" id="stock_id" name="per_unit"  value="<?php echo $info['per_unit'];?>"  placeholder="Enter Per Unit" type="number" required=""></td>
        </tr>

		      <tr>
        <td align="right">Status</td>
        <td>
           <select name="status" id="status">
                <option value="<?php echo $info['status'];?>"> <?php echo $info['status'];?> </option>
                <option value="active">Active</option>
                <option value="inactive">Inactive</option>
            </select>
        </td>
        </tr>


        <tr>
        <td align="right">Expiery Date</td>
        <td><input class="form-control form datepick" id="expire_date" name="expire_date" value="<?php echo $info['expire_date'];?>" type="date" value="<?php echo $info['expire_date'];?>"></td>
        </tr>

           <tr>
           <td valign="top" style="padding-top:20px;" align="right">Description</td>
           <td><textarea rows="5" cols="20" id="description" name="description" type="text"><?php echo $info['description'];?></textarea></td>
           </tr>

    	<td colspan="2" align="right">
			<input type="submit" class="view btn-success" value="Save">
		</td>
    	</tr>
    </table>
</form>
<?php }?>
