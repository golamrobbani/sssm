<?php
	include('pos.php');
	include('word.php');
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>COD Sales Report  </title>
    <style type="text/css" media="print">
        .hide {
            display: none
        }

    </style>
    <script type="text/javascript">
        function printpage() {
            document.getElementById('print_div').style.visibility = "hidden";
            window.print();
            document.getElementById('print_div').style.visibility = "visible";
        }
    </script>
    <style type="text/css">
        <!--
        .style1 {
            font-size: 10px
        }
        -->
    </style>
</head>
<body>
<div id="print_div" style="text-align:right;margin:50px auto;width:280px">
 

<input name="print" type="button" value="Print" id="printButton" onClick="printpage()">
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" valign="top">

            <table width="695" cellspacing="0" cellpadding="0" id="bordertable" border="1">
                <tr>
                    <td align="center"><h2>COD Sales Invoice </h2>
                        </strong>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%" align="left" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<?php 
									$id=$_GET['id'];
									$sql="select * from sales where id='$id'";
									$result=mysql_query($sql);
									while($data=mysql_fetch_array($result))
									{
										$name=$data['customer'];
										$address=$data['address'];
										$contact=$data['contact'];
										$date=$data['date'];
										$saleby=$data['saleby'];
										$gtotal=$data['gtotal'];
										$payable=$data['payable'];
										$tdiscount=$data['discount'];
										$paid=$data['paid'];
										$due=$data['due'];
										$cammount=$data['cammount'];
										$pmethod=$data['pmethod'];
										$pnumber=$data['pnumber'];
									}
									?>
									<tr>
										<td>Customer Name : </td>
										<td><?php echo $name; ?></td>
									</tr>
									<tr>
										<td>Customer Address: </td>
										<td><?php echo $address; ?></td>
									</tr>
									<tr>
										<td>Customer Contact:</td>
										<td><?php echo $contact; ?></td>
									</tr>
									
																		</table>
								</td>
                                <td align="right" width="50%">
                                  <table>
									<tr>
										<td>Date : </td>
										<td><?php echo $date; ?></td>
									</tr>
									 
									<tr>
										<td>Invoice No: </td>
										<td>BILL-<?php echo $id; ?></td>
									</tr>
									
									<tr>
										<td>Prepared By :  </td>
										<td>
											<?php echo $saleby; ?>									</td>
									</tr>
								  </table> 
                               </td>
										
                                
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" bgcolor="#CCCCCC">No</td>
                                <td align="center" bgcolor="#CCCCCC">Product</td>
                                <td align="center" bgcolor="#CCCCCC">Code</td>
                                <td align="center" bgcolor="#CCCCCC">Quantity</td>
                                <td align="center" bgcolor="#CCCCCC">Rate</td>
                                <td align="center" bgcolor="#CCCCCC">Discount</td>
                                <td align="center" bgcolor="#CCCCCC">Total</td>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <?php 
               
                $sql="select * from sales_product where sid='$id'";
                $result=mysql_query($sql);
				$count=mysql_num_rows($result);
 
				$x = 1;
				  
				while($x <= $count) {
				
 
                while($data=mysql_fetch_array($result))
                {
                    $name=$data['name'];
                    $quty=$data['quty'];
                    $sell=$data['sell'];
                    $discount=$data['discount'];
                    $total=$data['total'];
                   $unit=$data['unit'];
                   $pcode=$data['pcode'];
      ?>

                                                            <tr>
                                    <td align="center"><?php echo   $x  ; ?></td>
                                    <td align="center"><?php echo $name; ?></td>
                                    <td align="center"><?php echo $pcode; ?></td>
                                    <td align="center"><?php echo $quty; echo $unit; ?> </td>
                                    <td align="center"><?php echo $sell; ?> Tk </td>
                                    <td align="center"><?php echo $discount; ?> Tk </td>
                                    <td align="center"><?php echo $total; ?> Tk </td>
                                </tr>

                 



                <?php    
				  
				  $x++;
				} 
				
            }
                ?>          


                                                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
                                <td width="80%" align="right" bgcolor="#CCCCCC"><strong>Total Product:&nbsp;&nbsp;</strong>
                                </td>
                                <td width="20%" bgcolor="#CCCCCC"><?php echo $count; ?>	&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="80%" align="right" bgcolor="#CCCCCC"><strong>SubTotal:&nbsp;&nbsp;</strong>
                                </td>
                                <td width="20%" bgcolor="#CCCCCC"><?php echo $gtotal; ?>	 Tk &nbsp;</td>
                            </tr>
							<tr>
                                <td width="80%" align="right" bgcolor="#CCCCCC"><strong>Discount:&nbsp;&nbsp;</strong>
                                </td>

                                <td width="20%" bgcolor="#CCCCCC"><?php echo $tdiscount; ?>	 Tk &nbsp;</td>
                            </tr>
							                            <tr>
                                <td width="80%" align="right" bgcolor="#CCCCCC"><strong>Grand Total:&nbsp;&nbsp;</strong>
                                </td>
                                <td width="20%" bgcolor="#CCCCCC"><?php echo $payable; ?>	 Tk &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="45%" align="left" valign="top"><br/>
									&nbsp;&nbsp;Amount In Word :&nbsp;&nbsp;<strong style="text-transform:capitalize;"><?php echo convert_number_to_words($payable); ?> Taka Only</strong><br/>
                                    &nbsp;&nbsp;Paid Amount :&nbsp;&nbsp;<strong><?php echo $paid+$cammount; ?> Tk </strong><br/>									
                                       
										&nbsp;&nbsp;Change Amount :&nbsp;&nbsp;<strong><?php echo $cammount; ?> Tk </strong><br/>
                                        <!--&nbsp;&nbsp;Due Date&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;:-->  
                                    
									&nbsp;&nbsp;Payment Method :&nbsp;&nbsp;<strong><?php echo $pmethod; ?></strong><br/>
												<?php
		if($pmethod=='bkash')
		{
			?>
		&nbsp;&nbsp;Bkash Transition No :&nbsp;&nbsp;<strong><?php echo $pnumber; ?></strong><br/>
			<?php
		}
		elseif($pmethod=='giftcard')
		{
			?>
		&nbsp;&nbsp;Gift Card No :&nbsp;&nbsp;<strong><?php echo $pnumber; ?></strong><br/>
			<?php
		}
		elseif($pmethod=='card')
		{
			?>
		&nbsp;&nbsp;Debit/Credit Card No :&nbsp;&nbsp;<strong><?php echo $pnumber; ?></strong><br/>
			<?php
		}						
		?>
																		 <span style="font-size:25px">&nbsp;Due Balance
                                        :&nbsp;&nbsp;<strong><?php echo $due; ?> Tk </strong></span><br/>
									</td>
									
                                <td width="7%" align="right"><br/>
                                    <br/>
                                    <br/>
                                    </td>
                            </tr>
							
							
                        </table>
                    </td>
                </tr>
				<tr>
								<td>
									<table width="100%" style="height:100px">
										<tr>
											<td valign="bottom" align="right"><span style="border-top:1px solid #000">Authorised Signature</span></td>
										</tr>
									</table>
								</td>
							</tr>

            </table>
        </td>
    </tr>
</table>

</body>
</html>
