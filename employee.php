<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Employee Details</div>
		<div class="panel">
			<!--View-->
			<?php
				if($_POST)
				{
					$name = $_POST['name'];
					$phone = $_POST['phone'];
					$email = $_POST['email'];
					$address = $_POST['address'];
					$designation = $_POST['designation'];
					$salary = $_POST['salary'];
					$type = $_POST['type'];
					$date = date('d-m-Y');
					$date = strtotime($date);
					$req="INSERT INTO personinformation (date, name, phone, email, address, designation, salary, type)  VALUES ('$date','$name', '$phone', '$email', '$address', '$designation', '$salary', '$type')";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='employee.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='employee.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Employee Name</th>
						<th>Employee Designation</th>
						<th>Employee Phone</th>
						<th>Employee Email</th>
						<th>Employee Address</th>
						<th>Employee salary</th>
						<th><a href="employee_add.php" id="example1" class="view btn btn-primary">Add Employee</a></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysqli_query($conn, "SELECT * FROM  personinformation where type='employee' order by id DESC");

					while ($info = mysqli_fetch_array($query))
					{
					?>
					<tr>
						<td><?php echo $info['name'];?></td>
						<td><?php echo $info['designation'];?></td>
						<td><?php echo $info['phone'];?></td>
						<td><?php echo $info['email'];?></td>
						<td><?php echo $info['address'];?></td>
						<td>Tk  <?php echo $info['salary'];?></td>
						<td width="140">
							<span class="pull-right">
								<a title="View" href="employee_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="employee_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="employee_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
