<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Single Supplier Report</div>
		<div class="panel">

			<!--View-->
		 
			<div class="report_right">

			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">
           <input type="hidden" value="<?php echo $_GET['cid']; ?>" name="cid" />
						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
				</table>

				</form>

				<div class="table_data" id="mydiv">
					<table  id="table_id"  class="display table table-bordered">
					<thead>
						<tr>
							<th>Sl</th>
							<th>Date</th>
							<th>Description</th>
							<th>Quantity</th>
							<th>Rate</th>
							<th>Debit(qty*rate/Amount given)</th>
							<th>Credit(Amount Payment)</th>
							<th>Balance(Balance+dr-cr)</th>		
							<th>Comment</th>
						</tr>
					</thead>

					<tbody>
					<?php
      		
      		   	 $cid = $_GET['cid'];

		          // $recives = mysqli_query($conn, "SELECT sum(payment) as payment FROM payment where supplier_id='$cid' order by id desc");
		          // while ($recive = mysqli_fetch_array($recives)){
		          //   $payment = $recive['payment'];
		          // }

					if(isset($_GET['Submit']))
					{
						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to); 

						$balance = 0;  
						$x = 1;  
							$customer = mysqli_query($conn, "SELECT * FROM personinformation where type='Supplier' and id='$cid'"); 
							while ($cus = mysqli_fetch_array($customer))
							{
								$name = $cus['name'];
								$previous_due = $cus['due'];
							}

							$balance = $previous_due;
							?>

					<h4>Supplier Name: <?php echo $name; ?></h4>

					<tr>
						<td align="center"><?php echo $x; ?></td>
						<td align="center">-</td>
						<td align="center">BD Balance</td>
						<td align="center">-</td>
						<td align="center">-</td>
						<td align="center">-</td> 
						<td align="center">-</td>
						<td align="center"><?php echo number_format($previous_due, 2);?></td>
						<td align="center">-</td>
					</tr> 

					<?php

						$sps="SELECT * FROM ( (SELECT purchases_product.date, purchases_product.name, purchases_product.quty, purchases_product.sell, NULL as payment, NULL as note FROM purchases_product where supplier_id='$cid' and  date between '$from' and '$to') UNION ALL (SELECT payment.date, payment.name, NULL as quty, NULL as sell, payment.payment, payment.note FROM payment where supplier_id = '$cid' and  date between '$from' and '$to')) results ORDER BY date ASC";
						$rsps=mysqli_query($conn, $sps); 
						while($product=mysqli_fetch_array($rsps))
						{ 
						  $x = $x + 1;
						  $balance  +=  (($product['sell'] * $product['quty'])) -  $product['payment'];

						?>
					<tr>
						<td align="center"><?php echo $x; ?></td>
						<td align="center"><?php echo date("d-m-Y", $product['date']); ?></td>
						<td align="center"><?php echo $product['payment'] == 0 ? $product['name'] : "Cash Payment";?></td>
						<td align="center"><?php echo $product['quty'] == 0 ? "-" : $product['quty'];?></td>
						<td align="center"><?php echo $product['sell'] == 0 ? "-" : number_format($product['sell'], 2);?></td>
						<td align="center"><?php echo $product['sell'] == 0 ? "-" : number_format($product['sell'] * $product['quty'], 2);?></td> 
						<td align="center"><?php echo $product['sell'] != 0 ? "-" : number_format($product['payment'], 2);?></td>
						<td align="center"><?php echo number_format($balance, 2);?></td>
						<td align="center"><?php echo $product['note'] == '' ? '-' : $product['note'];?></td>
					</tr>

        <?php	 } 
        		 }
					else
					{

							$balance = 0;  
							$x = 1;  
							$customer = mysqli_query($conn, "SELECT * FROM personinformation where type='Supplier' and id='$cid'"); 
							while ($cus = mysqli_fetch_array($customer))
							{
								$name = $cus['name'];
								$previous_due = $cus['due'];
							}

							$balance = $previous_due;
							?>

					<h4>Supplier Name: <?php echo $name; ?></h4>

					<tr>
						<td align="center"><?php echo $x; ?></td>
						<td align="center">-</td>
						<td align="center">BD Balance</td>
						<td align="center">-</td>
						<td align="center">-</td>
						<td align="center">-</td> 
						<td align="center">-</td>
						<td align="center"><?php echo number_format($previous_due, 2);?></td>
						<td align="center">-</td>
					</tr> 

					<?php

						$sps="SELECT * FROM ( (SELECT purchases_product.date, purchases_product.name, purchases_product.quty, purchases_product.sell, NULL as payment, NULL as note FROM purchases_product where supplier_id='$cid') UNION ALL (SELECT payment.date, payment.name, NULL as quty, NULL as sell, payment.payment, payment.note FROM payment where supplier_id = '$cid')) results ORDER BY date ASC";
						$rsps=mysqli_query($conn, $sps); 
						while($product=mysqli_fetch_array($rsps))
						{ 
						  $x = $x + 1;
						  $balance  +=  (($product['sell'] * $product['quty'])) -  $product['payment'];

						?>
					<tr>
						<td align="center"><?php echo $x; ?></td>
						<td align="center"><?php echo date("d-m-Y", $product['date']); ?></td>
						<td align="center"><?php echo $product['payment'] == 0 ? $product['name'] : "Cash Payment";?></td>
						<td align="center"><?php echo $product['quty'] == 0 ? "-" : $product['quty'];?></td>
						<td align="center"><?php echo $product['sell'] == 0 ? "-" : number_format($product['sell'], 2);?></td>
						<td align="center"><?php echo $product['sell'] == 0 ? "-" : number_format($product['sell'] * $product['quty'], 2);?></td> 
						<td align="center"><?php echo $product['sell'] != 0 ? "-" : number_format($product['payment'], 2);?></td>
						<td align="center"><?php echo number_format($balance, 2);?></td>
						<td align="center"><?php echo $product['note'] == '' ? '-' : $product['note'];?></td>
					</tr>

			        <?php
			        	}  
			        	}
			    	?>
					</tbody>
				</table>

 
			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
