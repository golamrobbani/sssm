<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Earning Report</div>
		<div class="panel">
				<?php
				if($_POST)
				{
					$date = $_POST['date'];
					$date = str_replace('/', '-', $date);
					$date = strtotime($date);
					$comments = $_POST['comments'];
					$earning_head = $_POST['earning_head'];
					$amount = $_POST['amount'];
					$req="INSERT INTO earning (date, comments, earning_head, amount)  VALUES ('$date', '$comments', '$earning_head', '$amount')";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='report_earn.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='report_earn.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
			 	</table>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id" class="display table table-bordered">
					<thead>
						<tr>
						<th>Date</th>
						<th>Earning Head Name</th>
						<th>Earning Amount</th>
						<th>Comments</th>
				<th></th>	</tr></thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{
						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);

						$req = mysqli_query($conn, "SELECT sum(amount) FROM earning  where date between '$from' and '$to' order by id desc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(amount)'];

					}
					$purchase = mysqli_query($conn, "SELECT * FROM  earning where date between '$from' and '$to' order by id desc");
					while ($info = mysqli_fetch_array($purchase))
						{

					?>

					<tr>
						<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td><?php echo $info['earning_head'];?></td>
						<td>Tk <?php echo $info['amount'];?></td>
						<td><?php echo $info['comments'];?></td>

						<td width="150">
							<span class="pull-right">
								<a title="View" href="earning_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="earning_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="earning_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php		  } }
					else
					{

					$req = mysqli_query($conn, "SELECT sum(amount) FROM earning order by id desc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(amount)'];

					}
						$purchase = mysqli_query($conn, "SELECT * FROM  earning order by id desc");
					while ($info = mysqli_fetch_array($purchase))
					{

					?>

					<tr>

						<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td><?php echo $info['earning_head'];?></td>
						<td>Tk <?php echo $info['amount'];?></td>
						<td><?php echo $info['comments'];?></td>

						<td width="150">
							<span class="pull-right">
								<a title="View" href="earning_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
								<a title="Edit" href="earning_eidit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Edit</a>
								<a title="Delete" href="earning_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php    } } ?>
					</tbody>
				</table>
								<br />
	<br />
	<table>
		<tr>
			<th align="left">Total Earning : </th>
			<td align="right"><?php echo $payable; ?> Tk </td>
		</tr>

	</table>
			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
