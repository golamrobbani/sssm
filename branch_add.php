<form action="" method="POST" class="form">
    <table class="tab">
    	<tr>
			<td align="right">Branch Name</td>
			<td><input type="text" name="branch_name" id="branch_name" placeholder="Enter branch name"required></td>
    	</tr>
    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right">Branch Address</td>
    	<td><textarea placeholder="Enter Branch Address" name="branch_address" id="branch_address" rows="5" cols="20"></textarea></td>
    	</tr>
    	<tr>
    	<td align="right">Branch Manager Name</td>
    	<td>
    		<input type="text" name="branch_manager" id="branch_manager" placeholder="Enter branch manager name" required>
    	</td>
    	</tr>
    	<tr>
    	<td align="right">Branch Phone</td>
    	<td><input type="text" name="branch_phone" id="branch_phone" placeholder="Enter branch phone"></td>
    	</tr>

    	<tr>
    	<td align="right">Branch Email</td>
    	<td><input type="text" name="branch_email" id="branch_email" placeholder="Enter branch email"></td>
    	</tr>

    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right">Enter Branch Comments</td>
    	<td><textarea placeholder="Enter Comments" name="comments" id="comments" rows="5" cols="20"></textarea></td>
    	</tr>
    	<tr>
    	
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input onclick="return confirm('Are you sure?')" class="view btn-success" type="submit" value="Submit"></td>
    	</tr>
    </table>
</form>