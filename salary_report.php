<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Employee Sallary Report</div>
		<div class="panel">
			<?php
				if($_POST)
				{
					$name = $_POST['name'];
					$phone = $_POST['phone'];
					$email = $_POST['email'];
					$address = $_POST['address'];
					$designation = $_POST['designation'];
					$salary = $_POST['salary'];
					$type = $_POST['type'];
					$date = date('d-m-Y');
					$date = strtotime($date);
					$req="INSERT INTO personinformation (date,name, phone, email, address, designation, salary, type)  VALUES ('$date','$name', '$phone', '$email', '$address', '$designation', '$salary', '$type')";
					if (mysqli_query($conn, $req))
					{
						echo"<script>location.href='report_employee.php?message=success'</script>";
					}
					else
					{
						echo"<script>location.href='report_employee.php?message=error'</script>";
					}
				}
			?>
			<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
						<a value="Print Report" href="#" onclick="PrintElem('#mydiv')"  class="btn-add btn-warning pull-right">Print Report</a>
				</table>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id" class="display table table-bordered">
					<thead>
						<tr>
						<th>Date</th>
						<th>Name</th>
						<th>Designation</th>
						<th>Month</th>
						<th>Year</th>
						<th>Paid Salary</th>
						<th>Due Salary</th>
						<th>Bonus Salary</th>
						<th>Note</th>
						<th> Action</th>
						</tr>
					</thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{

						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);

					$req = mysqli_query($conn, "SELECT sum(amount) FROM salary where date between '$from' and '$to' order by id desc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(amount)'];

					}
					$purchase = mysqli_query($conn, "SELECT * FROM  salary where date between '$from' and '$to' order by id desc");
					while ($info = mysqli_fetch_array($purchase))
						{
				 $id=$info['employee_id'];
				$amount=$info['amount'];
				 $sq = mysqli_query($conn, "SELECT * FROM  personinformation where type='employee' and id='$id'");
					while ($data = mysqli_fetch_array($sq))
					{
						$name=$data['name'];
						$designation=$data['designation'];
						$salary=$data['salary'];
						 $dues=$salary-$amount;
				 	if($dues<0)
						{
							$due=0;
							$bonus=$amount-$salary;
						}
						else{
							$due=$dues;
							$bonus=0;
						}
					?>

					<tr>
					<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td><?php echo $name; ?></td>
						<td><?php echo $designation;?></td>
						<td><?php echo $info['month'];?></td>
						<td><?php echo $info['year'];?></td>
						<td>Tk <?php echo $info['amount'];?></td>

						<td>Tk  <?php echo $due;?></td>
						<td>Tk  <?php echo $bonus;?></td>
					 <td><?php echo $info['notes'];?></td>
						<td width="140">
							<span class="pull-right">
								<a title="View" href="salary_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
							   <a title="Delete" href="salary_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php		} } }
					else
					{
						$req = mysqli_query($conn, "SELECT sum(amount) FROM salary order by id desc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(amount)'];

					}
						$purchase = mysqli_query($conn, "SELECT * FROM  salary order by id desc");
					while ($info = mysqli_fetch_array($purchase))
					{
				 $id=$info['employee_id'];
				$amount=$info['amount'];
				 $sq = mysqli_query($conn, "SELECT * FROM  personinformation where type='employee' and id='$id'");
					while ($data = mysqli_fetch_array($sq))
					{
						$name=$data['name'];
						$designation=$data['designation'];
						$salary=$data['salary'];
						$dues=$salary-$amount;
				 	if($dues<0)
						{
							$due=0;
							$bonus=$amount-$salary;
						}
						else{
							$due=$dues;
							$bonus=0;
						}
					?>

					<tr>
						<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td><?php echo $name; ?></td>
						<td><?php echo $designation;?></td>
						<td><?php echo $info['month'];?></td>
						<td><?php echo $info['year'];?></td>
						<td>Tk <?php echo $info['amount'];?></td>

						<td>Tk  <?php echo $due;?></td>
						<td>Tk  <?php echo $bonus;?></td>
					 <td><?php echo $info['notes'];?></td>
						<td width="140">
							<span class="pull-right">
								<a title="Edit" href="salary_edit.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">Edit</a>
								<a title="View" href="salary_view.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>
							   <a title="Delete" href="salary_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php }  } } ?>
					</tbody>
				</table>
									<br />
	<br />
	<table>
		<tr>
			<th align="left">Total Salary : </th>
			<td align="right"><?php echo $payable; ?> Tk </td>
		</tr>

	</table>
			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
