<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Payment Details</div>
		<div class="panel">
			<!--View-->

			<table id="table_id" class="display table table-bordered">
				<thead>
					<tr>
						<th>Date</th>
						<th>Order No.</th>
						<th>Supplier Name</th>
						<th>Payable Amount</th>
						<th>Paid Amount</th>
						<th>Due Amount</th>

							<th>Payment Now</th>

					</tr>
				</thead>
				<tbody>
					<?php
					$query = mysqli_query($conn, "SELECT * FROM  purchases where due>0");
					while ($info = mysqli_fetch_array($query))
					{
						$sid = $info['supplier_id'];
						$id = $info['id'];
						$due = $info['due'];
					$q = mysqli_query($conn, "SELECT sum(payment) FROM  payment where supplier_id='$sid' and order_id='$id'");
					while ($in = mysqli_fetch_array($q))
					{
						$totalPaid = $in['sum(payment)'];
					}

					if($due > $totalPaid){



						?>
					<tr>
						<td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td>#<?php echo $info['id'];?></td>
						<td><?php echo $info['supplier'];?></td>
						<td>Tk <?php echo number_format($info['payable'],2);?></td>
						<td>Tk <?php echo number_format($info['paid'] + $totalPaid,2);?></td>
						<td>Tk <?php echo number_format($info['due'] - $totalPaid,2);?></td>

						<td>
							<span class="pull-right">
							  <a title="Payment" href="payment_now.php?id=<?php echo $info['id'];?>" id="example1" class="view btn-primary">Payment Now</a>
								</span>
							</span>
						</td>
					</tr>
				<?php } }?>
				</tbody>
			</table>
		</div>
	</div>
<?php include 'footer.php';?>
