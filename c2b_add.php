<?php
include 'pos.php';
?>
<form action="" method="POST" class="form">
    <table class="tab">
    	<tr>
    	<td align="right">Date</td>
    	<td><input type="text" name="date" id="date" value="<?php echo date('d/m/Y');?>" required></td>
    	</tr>
      <tr>
        <td align="right">Select Bank</td>
        <td>
      <select name="bank" id="earning_head" required>
        <option value="">--- Select ---</option>
        <?php
          $data=mysqli_query($conn, "SELECT * FROM bankinformation");
          while($data_info=mysqli_fetch_array($data))
          {
          $head_name= $data_info['bankname'];
          ?>
          <option value="<?php echo $data_info['id'];?>"><?php echo $head_name;?>(<?php echo $data_info['accountnumber'];?>)</option>
        <?php }?>
      </select>
      </td>
      </tr>
      
	    <tr>
    	<td align="right">Transfer Amount</td>
    	<td><input type="number" required=""  step="0.01" name="tamount" id="bname" ></td>
    	</tr>

    	<tr>
    	<td valign="top" style="padding-top:20px;" align="right">Comments</td>
    	<td><textarea placeholder="Enter Comments" name="comments" id="comments" rows="5" cols="20"></textarea></td>
    	</tr>
    	<tr>
    	<td colspan="2" align="right">
		<div id="hidden_field"></div>
		<input type="submit" class="view btn-success" value="Submit"></td>
    	</tr>
    </table>
</form>
