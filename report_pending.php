<?php include 'header.php';?>
	<div class="area">
		<div class="panel-head">Pending Payment Report</div>
		<div class="panel">
		<?php
				if (!empty($_GET['message']) && $_GET['message'] == 'success') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Inserted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'update') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Updated</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'delete') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Successfully Deleted</h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'error') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Your Data Uploaded Error ! </h4>';
					echo '</div>';
				}
				else if (!empty($_GET['message']) && $_GET['message'] == 'empty') {
					echo '<div class="alert alert-success">' ;
					echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo '<h4>Error ! Your Same Data Uploaded ... Are you want to edit? please select File </h4>';
					echo '</div>';
				}

			?>
			<!--View-->
			<div id='cssmenu' >
				<?php include 'report_menu.php';?>
			</div>
			<div class="report_right">
			   <form action="" method="get">
			   <table width="400px" class="tab form" border="0" cellspacing="0" cellpadding="0">

						<tr>
							<td width="2%">From</td>

							<td width="2%"><input class="form-control datepick" name="from" value="" type="text" id="from_sales_date"
									   style="width:160px;"></td>

							<td width="2%">To</td>

							<td width="2%"><input class="form-control datepick" name="to" value="" type="text" id="to_sales_date" style="width:160px;">
							</td>

							<td width="2%" valign="left"><input class="btn btn-info" type="submit" name="Submit" value="Show">
							</td>
						</tr>
			 	</table>
				</form>
				<div class="table_data" id="mydiv">
					<table  id="table_id" class="display table table-bordered">
					<thead>
						<tr>
							<th>Invoice Id</th>
							<th>Date</th>
							<th>Customer</th>
							<th>Payable</th> 
							<th>Paid</th>
							<th>Due</th>
							<th width="250">	<a href="#" id="exportBtns" onclick="exportTableToCSV('Pending Report.csv')">Export</a> </th>
						</tr>
					</thead>

					<tbody>
					<?php
					if(isset($_GET['Submit']))
					{
						$from = str_replace('/', '-', $_GET['from']);
						$to = str_replace('/', '-', $_GET['to']);

						$from = strtotime($from);
						$to = strtotime($to);

					$purchase = mysqli_query($conn, "SELECT * FROM  sales where status = 0 and date between '$from' and '$to' order by id desc");
					$req = mysqli_query($conn, "SELECT sum(payable),sum(due),sum(paid),sum(discount) FROM sales where status = 0 and date between '$from' and '$to' order by id desc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(payable)'];
				 	 $paid=$data['sum(paid)'];
				 	 $discount=$data['sum(discount)'];
				 	 $due=$data['sum(due)'];
					}

					while ($info = mysqli_fetch_array($purchase))
					{

					?>
					<tr>
						<td align="center">BILL-<?php echo $info['id'];?></td>
							 <td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td align="center"><?php echo $info['customer'];?></td>
						<td align="center">Tk <?php echo $info['payable'];?></td> 
						<td align="center">Tk <?php echo $info['paid'];?></td>
						<td align="center">Tk <?php echo $info['due'];?></td>

						<td>
							<span class="pull-right">

								<a title="Invoice"  target="_blank" href="sales_invoice.php?id=<?php echo $info['id'];?>"  class="view btn-info">Money Receipt</a>
								<a title="Chalan"  target="_blank" href="sales_chalan.php?id=<?php echo $info['id'];?>"  class="view btn-warning">Chalan</a>

								
								<a title="Confirm Payment" id="example1" href="confirm_payment.php?id=<?php echo $info['id'];?>"  class="view btn-info">Confirm Now</a>

									<a title="View" target="_blank" href="chalan.php?inv=<?php echo $info['id'];?>" id="example1" class="view btn-success">View</a>

									<a title="Edit"  href="edit_sale.php?id=<?php echo $info['id'];?>"  class="view btn-info" >Edit</a>

								<a title="Delete" href="report_sales_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php	 } }
					else
					{
					$purchase = mysqli_query($conn, "SELECT * FROM  sales where  status = 0  order by id desc  limit 10");
					$req = mysqli_query($conn, "SELECT sum(payable),sum(due),sum(paid),sum(discount) FROM  sales where   status = 0  order by id desc");

					while ($data = mysqli_fetch_array($req))
					{
				 	 $payable=$data['sum(payable)'];
				 	 $paid=$data['sum(paid)'];
				 	 $discount=$data['sum(discount)'];
				 	 $due=$data['sum(due)'];
					}
					while ($info = mysqli_fetch_array($purchase))
					{

					?>
					<tr>
						<td align="center">BILL-<?php echo $info['id'];?></td>
							 <td><?php echo date("d-m-Y", $info['date']); ?></td>
						<td align="center"><?php echo $info['customer'];?></td>
						<td align="center">Tk <?php echo $info['payable'];?></td> 
						<td align="center">Tk <?php echo $info['paid'];?></td>
						<td align="center">Tk <?php echo $info['due'];?></td>

						<td>
							<span class="pull-right">

								<a title="Invoice"  target="_blank" href="sales_invoice.php?id=<?php echo $info['id'];?>"  class="view btn-info">Money Receipt</a>
								<a title="Chalan"  target="_blank" href="sales_chalan.php?id=<?php echo $info['id'];?>"  class="view btn-warning">Chalan</a>


                					<a title="Confirm Payment" id="example1" href="confirm_payment.php?id=<?php echo $info['id'];?>"  class="view btn-info">Confirm Now</a>

									<a title="View" target="_blank" href="chalan.php?inv=<?php echo $info['id'];?>"  class="view btn-success">View</a>

									<a title="Edit"  href="edit_sale.php?id=<?php echo $info['id'];?>"  class="view btn-info" >Edit</a>

								<a title="Delete" href="report_sales_delete.php?id=<?php echo $info['id'];?>" onclick="return confirm('Are you sure?')" class="view btn-danger">Delete</a>
							</span>
						</td>
					</tr>
					<?php } }?>
					</tbody>
				</table>


			   </div>
			</div>
		</div>
	</div>
<?php include 'footer.php';?>
